package fr.soleil.comete.demo;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.Chart;

/**
 * A class used to test {@link Chart}. Displays a {@link JFrame} containing an empty {@link Chart}. You can then try to
 * load some data files to test {@link Chart} functionalities.
 * 
 * @author girardot
 */
public class ChartTest {

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(ChartTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Chart chart = new Chart();
        chart.setAutoHighlightOnLegend(true);
        chart.setDataViewRemovingEnabled(true);
        chart.setManagementPanelVisible(true);
        chart.setFreezePanelVisible(true);
        chart.setAxisSelectionVisible(true);
        chart.getChartView().getAxis(IChartViewer.Y1).getAttributes().setInverted(true);
        Map<String, Object> data = new HashMap<>();
        int dvCount = 3;
        int count = 50;
        for (int dvIndex = 0; dvIndex < dvCount; dvIndex++) {
            double[][] value = new double[2][];
            double[] x = new double[count], y = new double[count];
            for (int i = 0; i < count; i++) {
                x[i] = i;
                y[i] = Math.random() * 300;
            }
            value[IChartViewer.Y_INDEX] = y;
            value[IChartViewer.X_INDEX] = x;
            data.put("data " + (dvIndex + 1), value);
        }
        chart.setData(data);
        testFrame.setContentPane(chart);
        testFrame.setSize(700, 500);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

}
