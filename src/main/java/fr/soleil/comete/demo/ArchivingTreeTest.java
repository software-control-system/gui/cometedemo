package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.File;
import java.lang.reflect.Array;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.cdma.exception.DataAccessException;
import org.cdma.interfaces.IArray;
import org.cdma.interfaces.IDataItem;
import org.cdma.interfaces.IDataset;
import org.cdma.interfaces.IGroup;
import org.cdma.plugin.mambo.navigation.SoleilMamboDataset;

import fr.soleil.comete.definition.event.CometeMouseEvent;
import fr.soleil.comete.definition.listener.IMouseListener;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.util.BasicTreeNode;
import fr.soleil.comete.definition.widget.util.ITreeNode;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.NumberMatrixTable;
import fr.soleil.comete.swing.Tree;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.container.matrix.DoubleMatrix;

public class ArchivingTreeTest {

    private static boolean tablePanelOpen = false;

    public static final ArchivingTreeMouseListener archivingListener = new ArchivingTreeMouseListener();

    public static class ArchivingTreeMouseListener implements IMouseListener {

        @Override
        public void mouseChanged(CometeMouseEvent event) {
            if (event.getClicks() > 1) {
                if (event.getSource() instanceof Tree) {
                    Tree tree = (Tree) event.getSource();
                    ITreeNode[] nodes = tree.getSelectedNodes();
                    if (nodes.length == 1) {
                        ITreeNode node = nodes[0];
                        if (node.getData() != null && !tablePanelOpen) {
                            tablePanelOpen = true;
                            openTableDialog(node.getName(), (IArray) node.getData());
                        }
                    }
                }
            }
        }
    }

    private static JPanel initPanel() {

        final JPanel panel = new JPanel(new BorderLayout());
        final Tree tree = new Tree();

        URL fileURL = ArchivingCDMAPluginTest.class.getResource("testCouleurExpression.vc");
        File newFile = null;
        IDataset dataset = null;
        try {
            newFile = new File(fileURL.toURI());
            dataset = new SoleilMamboDataset(newFile);
            dataset.open();

        } catch (DataAccessException e1) {
            e1.printStackTrace();
        } catch (URISyntaxException e2) {
            e2.printStackTrace();
        }

        ITreeNode rootNode = new BasicTreeNode();
        constructTree(rootNode, dataset.getRootGroup());
        tree.setRootNode(rootNode);
        tree.addMouseListener(archivingListener);

        panel.add(tree, BorderLayout.CENTER);

        final JLabel label = new JLabel("Click here to loose focus");

        panel.add(label, BorderLayout.SOUTH);
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                label.grabFocus();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });

        return panel;
    }

    private static void constructTree(ITreeNode parentNode, IGroup group) {
        if ((parentNode != null) && (group != null)) {
            try {
                parentNode.setName(group.getName());
                for (IGroup tempGroup : group.getGroupList()) {
                    ITreeNode node = new BasicTreeNode();
                    parentNode.addNodes(node);
                    constructTree(node, tempGroup);
                }
                for (IDataItem dataItem : group.getDataItemList()) {
                    ITreeNode node = new BasicTreeNode();
                    node.setName(dataItem.getName());
                    node.setData(dataItem.getData());
                    parentNode.addNodes(node);
                }
            } catch (DataAccessException e) {
                e.printStackTrace();
            }
        }
    }

    private static void openTableDialog(String name, IArray data) {

        NumberMatrixTable table = new NumberMatrixTable();
        Chart viewer = new Chart();

        // Parse Object arrays into double arrays
        Object storageData = data.getStorage();
        Object finalData = null;
        if (storageData.getClass().isArray()) {
            Object[] storageArray = (Object[]) storageData;
            finalData = Array.newInstance(Double.TYPE, storageArray.length);
            double[] castedFinalArray = (double[]) finalData;
            for (int i = 0; i < storageArray.length; ++i) {
                Object subData = storageArray[i];
                if (subData instanceof Number) {
                    castedFinalArray[i] = ((Number) subData).doubleValue();
                }
            }
        }

        try {
            AbstractNumberMatrix<?> matrix = new DoubleMatrix();
            int[] shape = data.getShape();
            if (shape.length == 2) {
                matrix.setFlatValue(finalData, shape[0], shape[1]);
            }
            table.setData(matrix);

            // viewer.setAnnotation(IChartViewer.TIME_ANNO, IChartViewer.Y1);
            Map<String, Object> dataToChart = new HashMap<String, Object>();
            dataToChart.put(name, finalData);
            viewer.setData(dataToChart);

        } catch (Exception e) {
            e.printStackTrace();
        }

        IFrame frame = new Frame();

        final JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.setSize(600, 400);
        tabbedPane.setPreferredSize(tabbedPane.getSize());
        tabbedPane.setEnabled(true);

        tabbedPane.addTab("TABLE", table);
        tabbedPane.addTab("CHART", viewer);

        frame.setTitle("Tree With Archiving CDMA plugin");
        frame.setContentPane(tabbedPane);
        frame.setSize(500, 500);
        frame.setTitle(tabbedPane.getClass().getName());
        ((Frame) frame).addWindowStateListener(new WindowStateListener() {
            @Override
            public void windowStateChanged(WindowEvent event) {
                if (event.getNewState() == WindowEvent.WINDOW_CLOSED) {
                    tablePanelOpen = false;
                }
            }
        });
        frame.setVisible(true);
    }

    public static void main(String[] args) {

        IFrame frame = new Frame();
        // DataSourceProducerProvider.pushNewProducer(TangoDataSourceFactory.class);

        final JPanel f = initPanel();
        f.setSize(600, 400);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("Tree With Archiving CDMA plugin");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
