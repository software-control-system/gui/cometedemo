package fr.soleil.comete.demo;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class BooleanImageTest {

    public static void main(String[] args) {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan/boolean_image_ro");
        ImageViewer viewer = new ImageViewer();
        viewer.setAlwaysFitMaxSize(true);
        JFrame frame = new JFrame(BooleanImageTest.class.getSimpleName());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(viewer);
        frame.setSize(500, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        NumberMatrixBox box = new NumberMatrixBox();
        box.connectWidget(viewer, key);
    }

}
