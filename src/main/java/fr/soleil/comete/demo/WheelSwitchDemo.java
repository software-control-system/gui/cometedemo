package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class WheelSwitchDemo {

    public static void main(String[] args) {

        String competeAttributeName = "katy/gs/publisher/Double_Value";
        // String competeAttributeName = "tango/tangotest/1/short_scalar_ro";
        // String competeAttributeName = "tango/tangotest/1/double_image_ro";
        // String competeAttributeName = "tango/tangotest/1/double_spectrum_ro";
        // String competeAttributeName = "katy/test/motor/position";
        // String competeAttributeName = "i16-m-c06/op/mono1-mtp_fpitch.2/velocity";
        // String competeAttributeName = "FLO/TEST/PARSER.1/energy_w";
        // String competeAttributeName = "tango/tangotest/1";
        final NumberScalarBox numberBox = new NumberScalarBox();

        final WheelSwitch wheelswitch = new WheelSwitch();
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        panel.add(wheelswitch, BorderLayout.CENTER);

        final JComboBox<String> combo = new JComboBox<>();
        panel.add(combo, BorderLayout.SOUTH);
        combo.addItem("Double_Value");
        combo.addItem("energy");

        combo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                TangoKey key = new TangoKey();
                TangoKeyTool.registerWriteAttribute(key, "katy/gs/publisher", combo.getSelectedItem().toString());
                numberBox.connectWidget(wheelswitch, key);
            }
        });

        // bean.start();

        JFrame frame = new JFrame();
        frame.setContentPane(panel);
        frame.pack();
        frame.setTitle(competeAttributeName);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
