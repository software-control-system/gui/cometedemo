package fr.soleil.comete.demo;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class DoubleChartPanelDemo extends JTabbedPane {

    private static final long serialVersionUID = -2577161198735817958L;

    private final Chart chartViewer1;
    private final Chart chartViewer2;

    public DoubleChartPanelDemo() {
        chartViewer1 = new Chart();
        chartViewer1.setAutoHighlightOnLegend(true);

        chartViewer2 = new Chart();
        chartViewer2.setAutoHighlightOnLegend(true);

        add("Chart1", chartViewer1);
        add("Chart2", chartViewer2);

        ChartViewerBox box = new ChartViewerBox();
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "ICA/SALSA/FIT.1/fittedDataY");
        box.connectWidget(chartViewer1, key);
        box.connectWidget(chartViewer2, key);
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame();
        DoubleChartPanelDemo panel = new DoubleChartPanelDemo();

        panel.setSize(600, 600);
        panel.setPreferredSize(panel.getSize());
        panel.setEnabled(true);

        frame.setContentPane(panel);
        frame.setSize(600, 600);
        frame.setTitle(panel.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}
