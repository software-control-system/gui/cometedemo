package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.definition.widget.util.CometeRoi;
import fr.soleil.comete.definition.widget.util.RoiShape;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;

/**
 * A class used to test {@link ImageViewer} connection/disconnection through {@link NumberMatrixBox}
 * 
 * @author girardot
 */
public class ImageConnexionTest {

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(ImageConnexionTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        final TangoKey key1 = new TangoKey();
        TangoKeyTool.registerAttribute(key1, "tango/tangotest/titan/double_image");
        final TangoKey key2 = new TangoKey();
        TangoKeyTool.registerAttribute(key2, "tango/tangotest/titan/long_image_ro");
        final ImageViewer viewer = new ImageViewer();
        viewer.setApplicationId(CometeUtils.generateIdForClass(ImageConnexionTest.class));
        final NumberMatrixBox box = new NumberMatrixBox();
        box.setCleanWidgetOnDisconnect(viewer, false);
        box.connectWidget(viewer, key1);
        final ConstrainedCheckBox keySelectionBox = new ConstrainedCheckBox("Use 1st key");
        keySelectionBox.setSelected(true);
        keySelectionBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        box.disconnectWidgetFromAll(viewer);
                        box.connectWidget(viewer, keySelectionBox.isSelected() ? key1 : key2);
                    }
                });
            }
        });
        final ConstrainedCheckBox cleanCheckBox = new ConstrainedCheckBox("Clean on disconnect");
        cleanCheckBox.setSelected(box.isCleanWidgetOnDisconnect(viewer));
        cleanCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                box.setCleanWidgetOnDisconnect(viewer, cleanCheckBox.isSelected());
            }
        });

        CometeRoi cometeRoi = new CometeRoi("testRoi", RoiShape.LINE, 10, 15, 63, 100, 100, CometeColor.GREEN);
        viewer.addRoi(cometeRoi);

        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(cleanCheckBox, BorderLayout.NORTH);
        mainPanel.add(viewer, BorderLayout.CENTER);
        mainPanel.add(keySelectionBox, BorderLayout.SOUTH);
        testFrame.setContentPane(mainPanel);
        testFrame.setSize(750, 550);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

}
