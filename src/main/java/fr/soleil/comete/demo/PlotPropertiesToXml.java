package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.properties.PlotProperties;
import fr.soleil.comete.definition.widget.properties.xml.PlotPropertiesXmlManager;
import fr.soleil.comete.swing.Chart;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

public class PlotPropertiesToXml extends JPanel implements IChartViewerListener {

    private static final long serialVersionUID = -1556863628363420793L;
    private final Chart chartViewer1;
    private final JTextArea textArea1;
    private final JButton applyProperties;

    public PlotPropertiesToXml() {
        chartViewer1 = new Chart();
        chartViewer1.setAutoHighlightOnLegend(true);
        textArea1 = new JTextArea();
        applyProperties = new JButton("Apply");
        applyProperties.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                String textProp = textArea1.getText();
                try {
                    Node node = XMLUtils.getRootNodeFromFileContent(textProp);
                    PlotProperties newPlotXML = PlotPropertiesXmlManager.loadPlotProperties(node);
                    String newstringProp = PlotPropertiesXmlManager.toXmlString(newPlotXML);
                    System.out.println("XML PlotProperties=" + newstringProp);
                    chartViewer1.setDataViewPlotProperties("Data", newPlotXML);
                } catch (XMLWarning e) {

                    e.printStackTrace();
                }

            }
        });
        setLayout(new BorderLayout());
        add(applyProperties, BorderLayout.NORTH);
        add(chartViewer1, BorderLayout.CENTER);
        add(textArea1, BorderLayout.SOUTH);
        initData();

    }

    private void initData() {
        Map<String, Object> data = new HashMap<String, Object>();
        double[] values = new double[] { 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9 };
        data.put("Data", values);
        chartViewer1.setData(data);

        chartViewer1.setDataViewDisplayName("Data", "MyData Label");
        String plotPropertiesString = PlotPropertiesXmlManager.toXmlString(chartViewer1
                .getDataViewPlotProperties("Data"));
        System.out.println("PlotProperties=" + plotPropertiesString);
        textArea1.setText(plotPropertiesString);
        chartViewer1.addChartViewerListener(this);
    }

    @Override
    public void chartViewerChanged(ChartViewerEvent event) {
        String viewId = event.getViewId();
        if ((viewId != null) && !viewId.isEmpty()) {
            PlotProperties prop1 = chartViewer1.getDataViewPlotProperties(viewId);
            String stringProp = PlotPropertiesXmlManager.toXmlString(prop1);
            textArea1.setText(stringProp);
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        PlotPropertiesToXml panel = new PlotPropertiesToXml();
        panel.setSize(600, 600);
        panel.setPreferredSize(panel.getSize());
        panel.setEnabled(true);
        frame.setContentPane(panel);
        frame.setSize(600, 600);
        frame.setTitle(panel.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
