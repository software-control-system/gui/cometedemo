package fr.soleil.comete.demo;

import java.net.URL;

import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.cdma.data.keys.CDMAKey;
import fr.soleil.comete.cdma.data.service.CDMAKeyFactory;
import fr.soleil.comete.target.basic.BasicStringMatrixTarget;
import fr.soleil.data.target.matrix.ITextMatrixTarget;

public class ArchivingCDMAPluginTest {

    private static void testCDMAConnection() {

        try {
            CDMAKeyFactory factory = new CDMAKeyFactory();
            URL fileURL = ArchivingCDMAPluginTest.class.getResource("testCouleurExpression.vc");
            CDMAKey key = factory.generateKeyGroupList(fileURL.toURI(), new String[] {});

            ITextMatrixTarget target = new BasicStringMatrixTarget();
            StringMatrixBox stringMatrixBox = new StringMatrixBox();

            stringMatrixBox.connectWidget(target, key);

            String[] result = target.getFlatStringMatrix();
            System.out.println("--------------------");
            if (result != null) {
                for (String elem : result) {
                    System.out.println(elem);
                }
            }
            System.out.println("--------------------");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        testCDMAConnection();
    }
}
