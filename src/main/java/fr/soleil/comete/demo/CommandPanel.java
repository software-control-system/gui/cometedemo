package fr.soleil.comete.demo;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class CommandPanel extends JPanel {

    private static final long serialVersionUID = 6952493837849699753L;

    private final StringButton commandButton;
    private final StringScalarBox stringBox;

    public CommandPanel() {
        commandButton = new StringButton();
        commandButton.setButtonLook(true);
        stringBox = new StringScalarBox();
        add(commandButton);
    }

    public void connect() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerCommand(key, "tango/tangotest/titan", "State");
        commandButton.setText(key.getInformationKey());
        stringBox.setOutputInPopup(commandButton, true);
        stringBox.setUseErrorText(commandButton, false);
        stringBox.connectWidget(commandButton, key);
        stringBox.setConfirmation(commandButton, true);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Comete V2 " + CommandPanel.class.getSimpleName());
        CommandPanel commandPanel = new CommandPanel();
        commandPanel.connect();
        frame.setSize(400, 400);
        frame.setContentPane(commandPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }
}
