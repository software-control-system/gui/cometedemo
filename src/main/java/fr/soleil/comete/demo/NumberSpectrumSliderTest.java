package fr.soleil.comete.demo;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.comete.swing.Slider;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class NumberSpectrumSliderTest {
    private static final String DEVICENAME = "tango/tangotest/1";
    private static final NumberMatrixBox numberMatrixBox = new NumberMatrixBox();
    private static final ArrayPositionConvertor valueConvertor = new ArrayPositionConvertor();

    private final static Slider slider = new Slider();

    public static void main(String[] args) {

        // Build panel

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(slider, BorderLayout.CENTER);

        if (slider != null) {
            slider.setValueConvertor(valueConvertor);
        }

        slider.setMaximumDouble(15);
        slider.setMinimumDouble(0);
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.setPaintTrack(true);
        slider.setStep(1);
        slider.setOpaque(true);

        JFrame frame = new JFrame();
        frame.setContentPane(panel);
        frame.pack();
        frame.setSize(900, 200);
        frame.setTitle("TestSliderBug");
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        writeData();
        connectConvertor();
    }

    private static void connectConvertor() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, DEVICENAME, "double_spectrum");
        numberMatrixBox.connectWidget(valueConvertor, key);
        slider.revalidate();
        slider.repaint();
    }

    private static void writeData() {
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(DEVICENAME);

        if (proxy != null) {
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("double_spectrum");
                double[] value1 = new double[] { 51, 52, 63, 74, 95, 106, 107, 108, 109, 110, 112, 130, 140, 141, 144,
                        150 };
                deviceAttribute.insert(value1);
                proxy.write_attribute(deviceAttribute);

            } catch (DevFailed e) {
                e.printStackTrace();
            }
        }
    }
}
