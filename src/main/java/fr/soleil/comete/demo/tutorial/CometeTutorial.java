package fr.soleil.comete.demo.tutorial;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.EventObject;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.slf4j.LoggerFactory;

import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.controller.NumberMatrixCometeController;
import fr.soleil.comete.definition.data.controller.ChartViewerController;
import fr.soleil.comete.definition.listener.ITextFieldListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.definition.widget.util.AngularSector;
import fr.soleil.comete.demo.simulated.data.service.TangoKeyGenerator;
import fr.soleil.comete.simulated.data.service.IKeyGenerator;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.Panel;
import fr.soleil.comete.swing.TabbedPane;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.tango.data.source.attribute.StringAttributeDataSource;
import fr.soleil.data.container.matrix.AbstractNumberMatrix;
import fr.soleil.data.controller.BasicTextTargetController;
import fr.soleil.data.mediator.AbstractController;
import fr.soleil.data.mediator.Mediator;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.DataControllerProvider;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.GenericDescriptor;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.source.AbstractDataSource;
import fr.soleil.data.target.ITarget;
import fr.soleil.data.target.information.TargetInformation;

/**
 * Entry point of the Comete Tutorial
 * 
 * @author guerre-giordano
 *
 */
public class CometeTutorial extends TabbedPane {

    private static final long serialVersionUID = -3883994940428857043L;

    /**
     * Frame size
     */
    private static final int FRAME_SIZE = 800;

    /**
     * Mediators
     */
    private AbstractController<String> textFieldController;
    private AbstractController<Number> chartController;
    private AbstractController<AbstractNumberMatrix<?>> imageViewerController;
    /**
     * Tango key generator
     */
    private final IKeyGenerator tangoKey;

    /**
     * Application id for image viewer
     */
    private static int applicationIdNumber = 1;

    /**
     * Default constructor
     */
    public CometeTutorial() {
        super();

        // Tango key generator construction
        tangoKey = new TangoKeyGenerator();

        // Targets construction
        final TextField advancedCaseTextField = new TextField();
        advancedCaseTextField.setColumns(20);

        final TextField simpleCaseTextField = new TextField();
        simpleCaseTextField.setColumns(20);

        final Chart advancedChart = new Chart();
        advancedChart.setFreezePanelVisible(true);
        advancedChart.setManagementPanelVisible(true);
        advancedChart.setAutoHighlightOnLegend(true);
        advancedChart.setAxisSelectionVisible(true);
        advancedChart.setAutoHideViews(true);

        final Chart simpleChart = new Chart();
        simpleChart.setFreezePanelVisible(true);
        simpleChart.setManagementPanelVisible(true);
        simpleChart.setAutoHighlightOnLegend(true);
        simpleChart.setAxisSelectionVisible(true);
        simpleChart.setAutoHideViews(true);

        final ImageViewer advancedImageViewer = new ImageViewer();
        advancedImageViewer.setShowRoiInformationTable(true);
        advancedImageViewer.setUseMaskManagement(true);
        advancedImageViewer.registerSectorClass(AngularSector.class);

        final ImageViewer simpleImageViewer = new ImageViewer();
        simpleImageViewer.setShowRoiInformationTable(true);
        simpleImageViewer.setUseMaskManagement(true);
        simpleImageViewer.registerSectorClass(AngularSector.class);

        connectAttributeAndTangoTextfield(false, advancedCaseTextField);
        connectAttributeAndTangoTextfield(true, simpleCaseTextField);

        connectAttributeAndTangoChart(false, advancedChart);
        connectAttributeAndTangoChart(true, simpleChart);

        connectAttributeAndTangoImageViewer(false, advancedImageViewer);
        connectAttributeAndTangoImageViewer(true, simpleImageViewer);

        IPanel textFieldSimpleCasePanel = new Panel();
        textFieldSimpleCasePanel.add(simpleCaseTextField);
        add("TEXTFIELD - Simple case", textFieldSimpleCasePanel);

        IPanel textFieldAdvancedCasePanel = new Panel();
        textFieldAdvancedCasePanel.add(advancedCaseTextField);
        add("TEXTFIELD - Advanced case", textFieldAdvancedCasePanel);

        JPanel chartAdvancedCasePanel = new JPanel(new BorderLayout());
        chartAdvancedCasePanel.setBackground(Color.WHITE);
        chartAdvancedCasePanel.add(new JLabel("Chart", JLabel.CENTER), BorderLayout.NORTH);
        chartAdvancedCasePanel.add(advancedChart, BorderLayout.CENTER);

        add("CHART - Advanced case", chartAdvancedCasePanel);

        JPanel chartSimpleCasePanel = new JPanel(new BorderLayout());
        chartSimpleCasePanel.setBackground(Color.WHITE);
        chartSimpleCasePanel.add(new JLabel("Chart", JLabel.CENTER), BorderLayout.NORTH);
        chartSimpleCasePanel.add(simpleChart, BorderLayout.CENTER);

        add("CHART - Simple case", chartSimpleCasePanel);

        JPanel advancedCaseImageViewerPanel = new JPanel(new GridLayout(1, 2));
        advancedCaseImageViewerPanel.add(advancedImageViewer);
        JPanel advancedImagePanel = new JPanel(new BorderLayout());
        advancedImagePanel.add(advancedCaseImageViewerPanel, BorderLayout.CENTER);

        add("IMAGE VIEWER - Advanced case", advancedImagePanel);

        JPanel simpleCaseImageViewerPanel = new JPanel(new GridLayout(1, 2));
        simpleCaseImageViewerPanel.add(simpleImageViewer);
        JPanel simpleImagePanel = new JPanel(new BorderLayout());
        simpleImagePanel.add(simpleCaseImageViewerPanel, BorderLayout.CENTER);

        add("IMAGE VIEWER - Simple case", simpleImagePanel);

    }

    /**
     * Connect Tango attribute to an image viewer
     * 
     * @param simpleCase indicates whether or not the CometeBox facilities are used
     * @param imageViewer target
     */
    @SuppressWarnings("unchecked")
    private void connectAttributeAndTangoImageViewer(boolean simpleCase, ImageViewer imageViewer) {
        IKey imageKey = tangoKey.getImageNumberMatrixKey();

        if (simpleCase) {
            // CometeBox construction
            NumberMatrixBox numberMatrixBox = new NumberMatrixBox();

            numberMatrixBox.connectWidget(imageViewer, imageKey);
        } else {
            // Application id
            imageViewer.setApplicationId("ImageViewerBox_Viewer_" + applicationIdNumber);
            applicationIdNumber++;

            // Data source construction
            AbstractDataSource<AbstractNumberMatrix<?>> numberMatrixDataSource = null;

            final IDataSourceProducer producer = DataSourceProducerProvider.getProducer(imageKey.getSourceProduction());

            try {
                numberMatrixDataSource = (AbstractDataSource<AbstractNumberMatrix<?>>) producer
                        .createDataSource(imageKey);
            } catch (Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(
                        "Failed to create source for key: " + (imageKey == null ? null : imageKey.getInformationKey()),
                        e);
            }

            imageViewerController = new NumberMatrixCometeController();

            boolean result = imageViewerController.addLink(numberMatrixDataSource, imageViewer);

            if (result) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .info("Link successfully created betwen target image and data source "
                                + imageKey.getInformationKey());
            } else {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(
                        "Failed to create link betwen target image and data source " + imageKey.getInformationKey());
            }
        }

    }

    /**
     * Connect Tango attribute to a chart
     * 
     * @param simpleCase indicates whether or not the CometeBox facilities are used
     * @param chart target
     */
    @SuppressWarnings("unchecked")
    private void connectAttributeAndTangoChart(boolean simpleCase, IChartViewer chart) {
        IKey chartKey = tangoKey.getChartKey();

        if (simpleCase) {
            // CometeBox construction
            ChartViewerBox chartBox = new ChartViewerBox();

            chartBox.connectWidget(chart, chartKey);
        } else {
            // Data source construction
            AbstractDataSource<Number> numberMatrixDataSource = null;

            final IDataSourceProducer producer = DataSourceProducerProvider.getProducer(chartKey.getSourceProduction());

            try {
                numberMatrixDataSource = (AbstractDataSource<Number>) producer.createDataSource(chartKey);
            } catch (Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error(
                        "Failed to create source for key: " + (chartKey == null ? null : chartKey.getInformationKey()),
                        e);
            }

            // Source type descriptor
            GenericDescriptor sourceType = numberMatrixDataSource.getDataType();

            // Target type descriptor
            GenericDescriptor[] desc = new GenericDescriptor[] { new GenericDescriptor(String.class),
                    new GenericDescriptor(Object.class) };
            GenericDescriptor targetType = new GenericDescriptor(String.class, desc);

            // Mediator construction
            if (sourceType != null && targetType != null) {
                chartController = DataControllerProvider.getController(sourceType, targetType);
                if (chartController == null) {
                    chartController = new ChartViewerController<Number>(sourceType);
                    ((ChartViewerController<Number>) chartController).setMultiDataViewEnabled(true);
                    DataControllerProvider.pushNewController(chartController, sourceType, targetType);
                }
            }

            boolean result = chartController.addLink(numberMatrixDataSource, chart);

            if (result) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).info("Link successfully created betwen target Chart "
                        + "and data source " + chartKey.getInformationKey());
            } else {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to create link betwen target Chart "
                        + "and data source " + chartKey.getInformationKey());
            }
        }

    }

    /**
     * Connect Tango Attribute to a text field
     * 
     * @param simpleCase indicates whether or not the CometeBox facilities are used
     * @param textField target
     */
    private void connectAttributeAndTangoTextfield(boolean simpleCase, final TextField textField) {

        // Get a key corresponding to a data source of string scalars
        IKey stringScalarKey = tangoKey.getStringScalarKey();

        if (simpleCase) {
            // CometeBox construction
            StringScalarBox stringBox = new StringScalarBox();

            stringBox.connectWidget(textField, stringScalarKey);
        } else {

            // Data source construction
            StringAttributeDataSource stringAttributeDataSource = null;

            final IDataSourceProducer producer = DataSourceProducerProvider
                    .getProducer(stringScalarKey.getSourceProduction());
            try {
                stringAttributeDataSource = (StringAttributeDataSource) ((AbstractRefreshingManager<?>) producer)
                        .createDataSource(stringScalarKey, false, true);

            } catch (final Exception e) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS).error("Failed to create source for key: "
                        + (stringScalarKey == null ? null : stringScalarKey.getInformationKey()), e);
            }

            // Controller construction
            GenericDescriptor sourceAndTargetType = new GenericDescriptor(String.class);

            textFieldController = DataControllerProvider.getController(sourceAndTargetType, sourceAndTargetType);
            if (textFieldController == null) {
                textFieldController = new BasicTextTargetController() {
                    @Override
                    protected void setDataToTarget(ITarget target, String data, AbstractDataSource<String> source) {
                        super.setDataToTarget(target, data, source);

                    }

                    @Override
                    protected void setDataToGenericTarget(ITarget target, String data,
                            AbstractDataSource<String> source) {
                        super.setDataToGenericTarget(target, data, source);
                    }
                };
                DataControllerProvider.pushNewController(textFieldController, sourceAndTargetType, sourceAndTargetType);
            }

            boolean result = textFieldController.addLink(stringAttributeDataSource, textField);

            if (result) {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .info("Link successfully created betwen target TextField and data source "
                                + stringScalarKey.getInformationKey());
            } else {
                LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                        .error("Failed to create link betwen target TextField and data source "
                                + stringScalarKey.getInformationKey());
            }

            textField.addTextFieldListener(new ITextFieldListener() {

                @Override
                public void textChanged(EventObject event) {
                    textFieldController
                            .transmitTargetChange(new TargetInformation<String>(textField, textField.getText()));
                    LoggerFactory.getLogger(Mediator.LOGGER_ACCESS)
                            .info("Controller has been notified of target change");

                }

                @Override
                public void actionPerformed(EventObject event) {

                }
            });
        }

    }

    public static void main(String[] args) {
        final CometeTutorial mainpanel = new CometeTutorial();
        final JFrame frame = new JFrame("Comete V2 Tutorial");

        frame.setContentPane(mainpanel);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                frame.pack();
                frame.setSize(FRAME_SIZE, FRAME_SIZE);
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
                frame.toFront();
            }
        });
    }

}
