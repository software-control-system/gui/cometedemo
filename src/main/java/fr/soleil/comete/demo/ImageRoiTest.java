package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.util.ij.RoiAndPluginUtil;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.service.DataSourceProducerProvider;
import ij.gui.Roi;

public class ImageRoiTest {

    public static void main(String[] args) {
        final ImageViewer viewer = new ImageViewer() {
            private static final long serialVersionUID = 8555814457975234626L;

            @Override
            protected void initSelectionMenu() {
                super.initSelectionMenu();
                selectionMenu.add(containerFactory.createMenuItem(getAction(MODE_ANGLE_ACTION)));
            }
        };
        viewer.setAlwaysFitMaxSize(true);
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_image_ro");
        NumberMatrixBox box = new NumberMatrixBox();
        box.connectWidget(viewer, key);
        DataSourceProducerProvider.setRefreshingStrategy(key, null);
        JButton button = new JButton("Roi Test");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StringBuilder builder = new StringBuilder("Existing rois:");
                for (Roi roi : viewer.getRoiManager().getAllRois()) {
                    builder.append("------------------\nRoi:");
                    builder.append("\nRoi class: ").append(roi == null ? null : roi.getClass().getName());
                    try {
                        RoiAndPluginUtil.roiToAppendable(viewer, roi, builder.append("\n"));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                JOptionPane.showMessageDialog(viewer, builder);
            }
        });
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(viewer, BorderLayout.CENTER);
        mainPanel.add(button, BorderLayout.SOUTH);
        JFrame frame = new JFrame(ImageRoiTest.class.getSimpleName());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(mainPanel);
        frame.setSize(600, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
