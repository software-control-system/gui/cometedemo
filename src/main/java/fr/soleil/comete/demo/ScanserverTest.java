package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.util.Arrays;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;

public class ScanserverTest extends JPanel {

    private static final long serialVersionUID = -1653742217675424168L;

    // TANGO TEST PARAMETERS
    private final static String DEVICE = "ica/salsa/scan.1";

    private final ChartViewerBox chartBox;
    private final NumberMatrixBox imageBox;
    private final StringScalarBox stringBox;

    private final Config1D config1D;
    private final Config2D config2D;

    private final Chart chartViewer;
    private final ImageViewer imageViewerData1;
    private final ImageViewer imageViewerData1Bis;
    private final ImageViewer imageViewerData2;
    private final Label actuatorLabel;
    private final TextField actuatorField;
    private final Label runNameField;
    private final Label stateField;

    public ScanserverTest() {
        super(new BorderLayout());

        chartBox = new ChartViewerBox();
        imageBox = new NumberMatrixBox();
        stringBox = new StringScalarBox();

        config1D = new Config1D();
        config2D = new Config2D();

        chartViewer = new Chart();
        imageViewerData1 = new ImageViewer();
        imageViewerData1Bis = new ImageViewer();
        imageViewerData2 = new ImageViewer();
        actuatorLabel = new Label();
        actuatorField = new TextField();
        runNameField = new Label();
        stateField = new Label();

        JTabbedPane tabs = new JTabbedPane();

        imageViewerData1.setAlwaysFitMaxSize(true);
        imageViewerData1Bis.setAlwaysFitMaxSize(true);
        imageViewerData2.setAlwaysFitMaxSize(true);

        connectImage(imageViewerData1Bis, "data_01");
        connectImage(imageViewerData2, "data_02");

        tabs.addTab("ChartViewer", chartViewer);
        tabs.addTab("Image Data 01", imageViewerData1);
        tabs.addTab("Image Data 01Bis", imageViewerData1Bis);
        tabs.addTab("Image Data 02", imageViewerData2);

        add(tabs, BorderLayout.CENTER);
        add(initScalarPanel(), BorderLayout.NORTH);

        JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 20));

        JButton config1DButton = new JButton(createStartConfig1DAction());
        southPanel.add(config1DButton);
        // southPanel.add(config1DButton, BorderLayout.EAST);

        JButton config2DButton = new JButton(createStartConfig2DAction());
        southPanel.add(config2DButton);
        // southPanel.add(config2DButton, BorderLayout.CENTER);

        JButton stopButton = new JButton(createStopAction());
        // southPanel.add(stopButton, BorderLayout.WEST);
        southPanel.add(stopButton);

        JButton connectButton = new JButton(createConnectAction());
        southPanel.add(connectButton);

        add(southPanel, BorderLayout.SOUTH);
    }

    public void connectRunName() {
        TangoKey runNameKey = new TangoKey();
        TangoKeyTool.registerAttribute(runNameKey, DEVICE, "runName");

        TangoKey stateKey = new TangoKey();
        TangoKeyTool.registerAttribute(stateKey, DEVICE, "state");

        stringBox.connectWidget(runNameField, runNameKey);
        stringBox.connectWidget(stateField, stateKey);
    }

    private void connectViewer(final IChartViewer viewer) {
        disconnectViewer(viewer);

        TangoKey keyActuator_1_1 = new TangoKey();
        TangoKeyTool.registerAttribute(keyActuator_1_1, DEVICE, "actuator_1_1");

        TangoKey keyData01 = new TangoKey();
        TangoKeyTool.registerAttribute(keyData01, DEVICE, "data_01");

        TangoKey keyData02 = new TangoKey();
        TangoKeyTool.registerAttribute(keyData02, DEVICE, "data_02");

        TangoKey keyData03 = new TangoKey();
        TangoKeyTool.registerAttribute(keyData03, DEVICE, "data_03");

        TangoKey keyActuator_2_1 = new TangoKey();
        TangoKeyTool.registerAttribute(keyActuator_2_1, DEVICE, "actuator_2_1");

        TangoKey keyActuator_1_2 = new TangoKey();
        TangoKeyTool.registerAttribute(keyActuator_1_2, DEVICE, "actuator_1_2");

        TangoKey keyFitter = new TangoKey();
        TangoKeyTool.registerAttribute(keyFitter, "ICA/SALSA/FIT.1", "fittedDataY");

        TangoKey keyCrash = new TangoKey();
        TangoKeyTool.registerAttribute(keyCrash, "ICA/SALSA/FIT.1", "fittedFunctionParameters");

        IKey dualKey = ChartViewerBox.createDualKey(keyActuator_1_1, keyData03);
        chartBox.connectWidget(viewer, dualKey);
        // chartBox.connectWidget(viewer, keyData01);
        // chartBox.connectWidgetDual(viewer, keyX, keyY2);
        // chartBox.connectWidgetDual(viewer, keyX, keyFitter);
        // chartBox.connectWidgetDual(viewer, keyX, keyCrash);
        // chartBox.connectWidget(viewer, keyY1);
    }

    private void disconnectAll() {
        disconnectViewer(chartViewer);
        disconnectImage(imageViewerData1);
        disconnectImage(imageViewerData1);
        disconnectScalar();
    }

    private void connectAll() {
        connectViewer(chartViewer);
        connectImage(imageViewerData1, "data_01");
        disconnectImage(imageViewerData1);
        connectScalar();
    }

    private void disconnectViewer(final IChartViewer viewer) {
        chartBox.disconnectWidgetFromAll(viewer);
    }

    private void connectImage(final ImageViewer image, String attributeName) {
        disconnectImage(image);
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, DEVICE, attributeName);
        imageBox.connectWidget(image, key);
    }

    private void disconnectImage(final ImageViewer image) {
        imageBox.disconnectWidgetFromAll(image);
    }

    private void disconnectScalar() {
        stringBox.disconnectWidgetFromAll(actuatorLabel);
        stringBox.disconnectWidgetFromAll(actuatorField);
    }

    private void connectScalar() {
        disconnectScalar();
        TangoKey actuatorKey = new TangoKey();
        TangoKeyTool.registerAttribute(actuatorKey, "tango/tangotest/titan", "ampli");

        TangoKey actuatorWriteKey = new TangoKey();
        TangoKeyTool.registerWriteAttribute(actuatorWriteKey, "tango/tangotest/titan", "ampli");

        stringBox.connectWidget(actuatorLabel, actuatorKey);
        stringBox.connectWidget(actuatorField, actuatorWriteKey);
    }

    private JPanel initScalarPanel() {
        JPanel result = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 20));

        result.add(runNameField);
        result.add(stateField);
        result.add(actuatorLabel);
        result.add(actuatorField);

        return result;
    }

    private void setRefreshingPeriod(TangoKey key) {
        IDataSourceProducer producer = DataSourceProducerProvider
                .getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
        if ((producer instanceof AbstractRefreshingManager<?>) && (key != null)) {
            ((AbstractRefreshingManager<?>) producer).setRefreshingStrategy(key, new PolledRefreshingStrategy(100));
        }
    }

    private Action createStartConfig1DAction() {
        AbstractAction result = new AbstractAction("Start Config 1D") {

            private static final long serialVersionUID = 8934685734857806640L;

            @Override
            public void actionPerformed(ActionEvent e) {
                scanExecution(config1D, "root/CometeDemo1D");
                disconnectAll();
                connectAll();
            }
        };
        return result;
    }

    private Action createStartConfig2DAction() {
        AbstractAction result = new AbstractAction("Start Config 2D") {

            private static final long serialVersionUID = 8726733328445415031L;

            @Override
            public void actionPerformed(ActionEvent e) {
                scanExecution(config2D, "root/CometeDemo2D");
                disconnectAll();
                connectAll();
            }
        };
        return result;
    }

    private Action createStopAction() {
        AbstractAction result = new AbstractAction("Stop") {

            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(DEVICE);
                    stopScan(proxy);
                } catch (DevFailed e1) {
                    System.out.println(Except.str_exception(e1));
                }
            }
        };
        return result;
    }

    private Action createConnectAction() {
        AbstractAction result = new AbstractAction("Connect") {

            private static final long serialVersionUID = 1L;

            @Override
            public void actionPerformed(ActionEvent e) {
                TangoKey keyData01 = new TangoKey();
                TangoKeyTool.registerAttribute(keyData01, DEVICE, "data_01");
                chartBox.connectWidget(chartViewer, keyData01);
                String id = keyData01.getInformationKey();
                IDataSourceProducer producer = DataSourceProducerProvider
                        .getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
                int[] shape = producer.getShape(keyData01);
                System.out.println("Shape =" + Arrays.toString(shape));
                for (int i = 0; i < shape[0]; i++) {
                    chartViewer.setDataViewAxis(id + "_" + i, IChartViewer.Y2);
                }
                chartBox.setSplitMatrixThroughColumns(keyData01, false);
                setRefreshingPeriod(keyData01);
            }
        };
        return result;
    }

    private void scanExecution(Config1D config, String runName) {
        try {
            DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(DEVICE);
            stopScan(proxy);
            cleanScan(proxy);
            config.installConfig(proxy);
            installRunName(proxy, runName);
            startScan(proxy);
        } catch (DevFailed e1) {
            System.out.println(Except.str_exception(e1));
        }
    }

    private void startScan(DeviceProxy proxy) throws DevFailed {
        if (proxy != null) {
            DeviceAttribute deviceAttr = new DeviceAttribute("recordData", false);
            proxy.write_attribute(deviceAttr);
            proxy.command_inout("Start");
        }
    }

    private void stopScan(DeviceProxy proxy) throws DevFailed {
        if (proxy != null) {
            proxy.command_inout("Abort");
        }
    }

    private void cleanScan(DeviceProxy proxy) throws DevFailed {
        if (proxy != null) {
            proxy.command_inout("Init");
        }
    }

    private void installRunName(DeviceProxy proxy, String name) throws DevFailed {
        if (proxy != null) {
            DeviceAttribute deviceAttr = new DeviceAttribute("runName", name);
            proxy.write_attribute(deviceAttr);
        }
    }

    public static void main(String[] args) {
        System.setProperty("TANGO_HOST", "calypso:20001");

        ScanserverTest tester = new ScanserverTest();

        JFrame frame = new JFrame("Testing COMETE on " + ScanserverTest.DEVICE);
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setContentPane(tester);
        frame.setSize(900, 800);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        tester.connectRunName();
        tester.connectAll();
    }

    // ////////
    // CONFIG /
    // ////////
    public class Config1D {

        // protected double actuatorsDelay = 0.0;
        // protected double actuatorsRetryTimeout = 0.2;
        // protected double actuatorsTimeOut = 5.0;
        // protected double afterRunActionActuatorValue = 0.0;
        // protected long actuatorsErrorStrategy = 0;
        // protected long actuatorsRetryCount = 0;
        // protected long afterRunActionSensor = 0;
        // protected long afterRunActionType = 0;
        // protected long afterRunActionActuator = 0;
        // protected long contextValidationErrorStrategy = 0;
        // protected boolean automaticDirection = false;
        // protected boolean dataRecorderPartialMode = false;
        // protected boolean enableScanSpeed = true;
        // protected boolean zigzag = false;

        protected String[] actuators;
        protected String[] sensors;
        protected double[] integrationTimes;
        protected double[] trajectories;

        /**
         * Constructor
         */
        public Config1D() {
            actuators = new String[] { "tango/tangotest/titan/ampli", "tango/tangotest/titan/short_scalar_w" };
            sensors = new String[] { "tango/tangotest/titan/long_scalar", "tango/tangotest/titan/short_scalar",
                    "tango/tangotest/1/double_scalar", "tango/tangotest/1/long_scalar" };
            // trajectories = new double[] { -1.0000000001, -1.0000000002, -2.0,
            // -2.5, -3.0, -3.5,
            // -4.0, -4.5, -5.0, -5.5, -6.0, -9.0, -11.0, -13.0, -15.0, -17.0,
            // -19.0, -1.0,
            // -1.9, -2.8, -3.7, -4.6, -5.5, -6.4, -7.3, -8.2, -9.1, -10.0,
            // -20.0, -21.0,
            // -22.0, -23.0, -24.0, -25.0 };
            int nbPoint = 4000;
            trajectories = new double[nbPoint * actuators.length];
            for (int i = 0; i < trajectories.length; i++) {
                trajectories[i] = i;
            }
            integrationTimes = new double[trajectories.length / actuators.length];
            Arrays.fill(integrationTimes, 1.0);
        }

        public void installConfig(DeviceProxy proxy) throws DevFailed {
            if (proxy != null) {
                DeviceAttribute deviceAttr = new DeviceAttribute("actuators");
                deviceAttr.insert(actuators);
                proxy.write_attribute(deviceAttr);

                deviceAttr = new DeviceAttribute("sensors");
                deviceAttr.insert(sensors);
                proxy.write_attribute(deviceAttr);

                deviceAttr = new DeviceAttribute("integrationTimes");
                deviceAttr.insert(integrationTimes);
                proxy.write_attribute(deviceAttr);

                deviceAttr = new DeviceAttribute("trajectories");
                deviceAttr.insert(trajectories, trajectories.length / actuators.length, actuators.length);
                proxy.write_attribute(deviceAttr);
            }
        }
    }

    public class Config2D extends Config1D {

        protected String[] actuators2;
        protected double[] trajectories2;

        public Config2D() {
            actuators = new String[] { "tango/tangotest/titan/ampli" };
            actuators2 = new String[] { "tango/tangotest/titan/short_scalar_w", "tango/tangotest/1/ampli" };
            trajectories = new double[] { 0.0, 2.5, 5.0, 7.5, 10.0, 11 };
            trajectories2 = new double[] { 0.0, 3.3333333333333335, 6.666666666666667, 10.0 };
            integrationTimes = new double[6];
            Arrays.fill(integrationTimes, 1.0);
        }

        @Override
        public void installConfig(DeviceProxy proxy) throws DevFailed {
            if (proxy != null) {
                super.installConfig(proxy);

                DeviceAttribute deviceAttr = new DeviceAttribute("actuators2");
                deviceAttr.insert(actuators2);
                proxy.write_attribute(deviceAttr);

                deviceAttr = new DeviceAttribute("trajectories2");
                deviceAttr.insert(trajectories2, trajectories2.length / actuators2.length, actuators2.length);
                proxy.write_attribute(deviceAttr);
            }
        }

    }

}
