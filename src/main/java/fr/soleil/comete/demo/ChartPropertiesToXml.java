package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.event.ChartViewerEvent;
import fr.soleil.comete.definition.event.ChartViewerEvent.Reason;
import fr.soleil.comete.definition.listener.IChartViewerListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.properties.ChartProperties;
import fr.soleil.comete.definition.widget.properties.xml.ChartPropertiesXmlManager;
import fr.soleil.comete.swing.Chart;
import fr.soleil.lib.project.date.IDateFormattable;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

public class ChartPropertiesToXml extends JSplitPane implements IChartViewerListener {

    private static final long serialVersionUID = -1293286200914049616L;

    private final Chart chartViewer1;
    private final Chart chartViewer2;
    private final JTextArea textArea1;
    private final JTextArea textArea2;
    private final JPanel panel1 = new JPanel();
    private final JPanel panel2 = new JPanel();
    private boolean ignoreEvent = false;
    private final JButton reload = new JButton("Reload");

    public ChartPropertiesToXml() {
        super();
        setOneTouchExpandable(true);
        ConstrainedCheckBox addRemoveListener = new ConstrainedCheckBox("isListening event");
        addRemoveListener.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                ConstrainedCheckBox source = (ConstrainedCheckBox) arg0.getSource();
                if (source.isSelected()) {
                    addChartViewerListener();
                } else {
                    removeChartViewerListener();
                }
            }
        });
        chartViewer1 = new Chart();
        chartViewer1.setAutoHighlightOnLegend(true);
        chartViewer2 = new Chart();
        chartViewer2.setAutoHighlightOnLegend(true);
        chartViewer1.setAxisLabelFormat(IChartViewer.TIME_FORMAT, IChartViewer.X);
        chartViewer1.setAxisDateFormat(IDateFormattable.US_DATE_FORMAT, IChartViewer.X);
        // chartViewer1.setAnnotation(IChartViewer.TIME_ANNO,
        // IChartViewer.X_INDEX);
        textArea1 = new JTextArea();
        textArea2 = new JTextArea();
        // chartViewer1.addExpression(dataViewName, expression, axis, variables,
        // x);
        // chartViewer1.setDataViewColor(dataViewName, color)
        panel1.setLayout(new BorderLayout());
        JPanel northPanel = new JPanel();
        northPanel.add(addRemoveListener);
        northPanel.add(reload);
        panel1.add(northPanel, BorderLayout.NORTH);
        panel1.add(chartViewer1, BorderLayout.CENTER);
        panel1.add(new JScrollPane(textArea1), BorderLayout.SOUTH);

        panel2.setLayout(new BorderLayout());
        panel2.add(chartViewer2, BorderLayout.CENTER);
        panel2.add(new JScrollPane(textArea2), BorderLayout.SOUTH);

        reload.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String chartString = textArea2.getText();
                    Node node = XMLUtils.getRootNodeFromFileContent(chartString);
                    ChartProperties prop = ChartPropertiesXmlManager.loadChartProperties(node);
                    chartViewer2.setChartProperties(prop);
                } catch (XMLWarning ex) {
                    // TODO Auto-generated catch block
                    ex.printStackTrace();
                }

            }
        });
        // setLayout(new GridBagLayout());

        // // Cell 00 Chart1
        // GridBagConstraints c00 = new GridBagConstraints();
        // c00.gridx = 0;
        // c00.gridy = 0;
        // c00.fill = GridBagConstraints.BOTH;
        // add(panel1, c00);
        //
        // // Cell 10 Chart2
        // GridBagConstraints c10 = new GridBagConstraints();
        // c10.gridx = 1;
        // c10.gridy = 0;
        // c10.fill = GridBagConstraints.BOTH;
        // add(panel2, c10);
        setLeftComponent(panel1);
        setRightComponent(panel2);

        initData();

    }

    private void addChartViewerListener() {
        chartViewer1.addChartViewerListener(this);
        chartViewer2.addChartViewerListener(this);
    }

    private void removeChartViewerListener() {
        chartViewer1.removeChartViewerListener(this);
        chartViewer2.removeChartViewerListener(this);
    }

    private void initData() {
        Map<String, Object> data = new HashMap<String, Object>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        long time = calendar.get(Calendar.HOUR_OF_DAY);

        double[] values = new double[] { time, 0, time + 1, 1, time + 2, 2, time + 3, 3, time + 4, 4, time + 5, 5,
                time + 6, 6, time + 7, 7, time + 8, 8, time + 9, 9 };
        data.put("Data", values);
        chartViewer1.setData(data);
        chartViewer2.setData(data);

        String string1 = ChartPropertiesXmlManager.toXmlString(chartViewer1.getChartProperties());
        String string2 = ChartPropertiesXmlManager.toXmlString(chartViewer2.getChartProperties());

        textArea1.setText(string1);
        textArea2.setText(string2);
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame();
        ChartPropertiesToXml panel = new ChartPropertiesToXml();

        panel.setSize(600, 600);
        panel.setPreferredSize(panel.getSize());
        panel.setEnabled(true);

        frame.setContentPane(panel);
        frame.setSize(600, 600);
        frame.setTitle(panel.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

    @Override
    public void chartViewerChanged(ChartViewerEvent event) {
        if (event.getReason() == Reason.CONFIGURATION) {
            System.out.println("dataViewId=" + event.getViewId());
        }

        if (!ignoreEvent) {
            ignoreEvent = true;
            IComponent source = event.getSource();
            ChartProperties prop2 = chartViewer2.getChartProperties();
            ChartProperties prop1 = chartViewer1.getChartProperties();

            if (source == chartViewer2) {
                chartViewer1.setChartProperties(prop2);
            } else if (source == chartViewer1) {
                chartViewer2.setChartProperties(prop1);
            }
            String string1 = ChartPropertiesXmlManager.toXmlString(prop1);
            String string2 = ChartPropertiesXmlManager.toXmlString(prop2);

            textArea1.setText(string1);
            textArea2.setText(string2);
            System.out.println("chartProperties1=\n" + string1);
            System.out.println("chartProperties2=\n" + string2);

            if ((string1 != null && string2 != null) && !string1.equals(string2)) {
                textArea1.setBackground(Color.CYAN);
                textArea2.setBackground(Color.CYAN);
            } else {
                textArea1.setBackground(Color.WHITE);
                textArea2.setBackground(Color.WHITE);
            }
            ignoreEvent = false;
        }

    }
}
