package fr.soleil.comete.demo;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.component.DualStringButton;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.util.ImageTool;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class DualButtonTest {

    public static void main(String[] args) {
        final JFrame testFrame = new JFrame(DualButtonTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel panel = new JPanel(new BorderLayout());
        DualStringButton button = new DualStringButton();
        StringScalarBox box = new StringScalarBox();
        Label statusLabel = new Label();
        String device = "tango/tangotest/1";
        JLabel deviceLabel = new JLabel(device);
        String command1 = "SwitchStates", command2 = "Init";
        TangoKey key1 = new TangoKey();
        TangoKeyTool.registerCommand(key1, device, command1);
        TangoKey key2 = new TangoKey();
        TangoKeyTool.registerCommand(key2, device, command2);
        TangoKey key3 = new TangoKey();
        TangoKeyTool.registerAttribute(key3, device, "status");
        ImageIcon icon1 = new ImageIcon(
                DualButtonTest.class.getResource("/org/tango-project/tango-icon-theme/16x16/actions/process-stop.png"));
        ImageIcon icon2 = new ImageIcon(
                DualButtonTest.class.getResource("/org/tango-project/tango-icon-theme/16x16/actions/view-refresh.png"));
        button.getFirstTarget().setText(command1);
        button.getFirstTarget().setCometeImage(ImageTool.getCometeImage(icon1));
        button.getSecondTarget().setText(command2);
        button.getSecondTarget().setCometeImage(ImageTool.getCometeImage(icon2));
        box.connectWidget(button.getFirstTarget(), key1);
        box.connectWidget(button.getSecondTarget(), key2);
        box.connectWidget(statusLabel, key3);
        panel.add(deviceLabel, BorderLayout.NORTH);
        panel.add(button, BorderLayout.CENTER);
        panel.add(statusLabel, BorderLayout.SOUTH);
        testFrame.setContentPane(panel);
        testFrame.pack();
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                testFrame.toFront();
            }
        });
    }

}
