package fr.soleil.comete.demo;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.Chart;

/**
 * A class used to test chart scales, to check Mantis #14767
 * 
 * @author girardot
 */
public class ChartScaleTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        Chart chart = new Chart();
        chart.setHeader("Chart V2");
        Map<String, Object> data = new HashMap<>();
        int length = 20;
        double[][] value = new double[2][length];
        for (int i = 0; i < length; i++) {
            value[IChartViewer.X_INDEX][i] = Double.NaN;
            value[IChartViewer.Y_INDEX][i] = 1.05 * i * Math.random();
        }
        data.put("Scale test", value);
        chart.setData(data);
        JFrame testFrame = new JFrame(ChartScaleTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(chart);
        testFrame.setSize(600, 400);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }
}
