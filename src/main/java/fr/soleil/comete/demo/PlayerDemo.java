package fr.soleil.comete.demo;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.definition.event.PlayerEvent;
import fr.soleil.comete.definition.listener.IPlayerListener;
import fr.soleil.comete.swing.Player;

public class PlayerDemo {

    public static void main(String[] args) {

        final Player player = new Player();
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        panel.add(player, BorderLayout.CENTER);
        player.addPlayerListener(new IPlayerListener() {

            @Override
            public void indexChanged(PlayerEvent event) {
                System.out.println("index changed =" + player.getIndex());
            }
        });

        player.initSlider(10);
        // bean.start();

        JFrame frame = new JFrame();
        panel.setSize(1500, 300);
        frame.setSize(1500, 300);
        frame.setContentPane(panel);
        frame.setTitle("Player Demo");

        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
