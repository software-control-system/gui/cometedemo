package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.definition.listener.ITableListener;
import fr.soleil.comete.swing.StringMatrixArea;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.container.matrix.AbstractMatrix;
import fr.soleil.data.container.matrix.StringMatrix;

public class StringMatrixAreaTest extends JPanel {

    private static final long serialVersionUID = 5632514725408268835L;

    private final static StringMatrixBox stringMatrixBox = new StringMatrixBox();
    private static final String DEVICENAME = "tango/tangotest/1";
    private static StringMatrixArea stringMatrixArea;
    private static String strImage = "string_image";
    private static String strSpectrum = "string_spectrum";

    private static void connectWidget(String attributeName) {
        stringMatrixBox.disconnectWidgetFromAll(stringMatrixArea);
        TangoKey key = new TangoKey();
        TangoKeyTool.registerWriteAttribute(key, DEVICENAME, attributeName);
        stringMatrixBox.connectWidget(stringMatrixArea, key);
    }

    public static void main(String[] args) {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        stringMatrixArea = new StringMatrixArea();
        mainPanel.add(stringMatrixArea, BorderLayout.CENTER);

        final JComboBox<String> jComboBox = new JComboBox<>(new String[] { strSpectrum, strImage });
        mainPanel.add(jComboBox, BorderLayout.SOUTH);

        jComboBox.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String attributeName = jComboBox.getSelectedItem().toString();
                connectWidget(attributeName);
            }
        });

        JFrame frame = new JFrame();
        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setTitle("StringMatrixAreaTest");
        frame.setAlwaysOnTop(true);
        frame.setSize(500, 500);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.toFront();
        ITableListener<AbstractMatrix<?>> iTableListener = new ITableListener<AbstractMatrix<?>>() {

            @Override
            public void valueChanged(AbstractMatrix<?> matrix) {
                if (matrix instanceof StringMatrix) {
                }
            }

            @Override
            public void selectedPointChanged(double[] point) {

            }

            @Override
            public void selectedColumnChanged(int col) {

            }

            @Override
            public void selectedRowChanged(int row) {

            }

        };
        stringMatrixArea.addTableListener(iTableListener);
        connectWidget(strSpectrum);
    }

}
