package fr.soleil.comete.demo.simulated.data.service;

import fr.soleil.comete.simulated.data.service.IKeyGenerator;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.service.IKey;

public class TangoKeyGenerator implements IKeyGenerator {

    public TangoKeyGenerator() {
    }

    @Override
    public IKey getBooleanMatrixKey() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "boolean_image_ro");
        return key;
    }

    @Override
    public IKey getBooleanScalarKey() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "boolean_scalar");
        return key;
    }

    @Override
    public IKey getChartKey() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_spectrum_ro");
        return key;
    }

    @Override
    public IKey getSpectrumNumberMatrixKey() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_spectrum_ro");
        return key;
    }

    @Override
    public IKey getImageNumberMatrixKey() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "ushort_image_ro");
        return key;
    }

    @Override
    public IKey getNumberScalarKey() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_scalar");
        return key;
    }

    @Override
    public IKey getStringMatrixKey() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "string_image_ro");
        return key;
    }

    @Override
    public IKey getStringScalarKey() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "string_scalar");
        return key;
    }

    @Override
    public String toString() {
        return TangoDataSourceFactory.SOURCE_PRODUCER_ID;
    }

    @Override
    public IKey getStringScalarKey2() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerCommand(key, "tango/tangotest/titan", "Status");
        return key;
    }

    @Override
    public IKey getNumberMatrixListKey() {
        // not yet managed
        return null;
    }

    @Override
    public IKey getChartListKey() {
        // not yet managed
        return null;
    }

}
