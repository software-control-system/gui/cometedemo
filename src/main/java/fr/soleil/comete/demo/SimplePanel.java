package fr.soleil.comete.demo;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.simulated.data.service.IKeyGenerator;
import fr.soleil.comete.simulated.data.service.SimulatedKeyGenerator;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.NumberTable;
import fr.soleil.comete.swing.Player;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class SimplePanel extends JPanel {

    private static final long serialVersionUID = -3873667866534097319L;

    private final Label state1 = new Label();
    private final Label state2 = new Label();
    private final StringScalarBox stringBox = new StringScalarBox();
    private final NumberMatrixBox matrixBox = new NumberMatrixBox();
    private final TextField textField = new TextField();
    private final NumberTable table = new NumberTable();
    private final ComboBox combo = new ComboBox();
    private final TextField textField2 = new TextField();
    private final Player player = new Player();

    public SimplePanel() {
        add(state1);
        add(state2);
        add(textField);
        add(table);
        add(combo);
        add(textField2);
        add(player);
    }

    public void connect() {
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan/State");
        stringBox.connectWidget(state1, key);

        IKeyGenerator simulatedKey = new SimulatedKeyGenerator();
        stringBox.connectWidget(state2, simulatedKey.getStringScalarKey());
        stringBox.connectWidget(textField, simulatedKey.getStringScalarKey());

        TangoKey attributeWritekey = new TangoKey();
        TangoKeyTool.registerWriteAttribute(attributeWritekey, "tango/tangotest/titan", "ampli");
        stringBox.connectWidget(combo, attributeWritekey);
        stringBox.setConfirmation(combo, true);
        combo.setDisplayedList(new String[] { "Valeur 0", "Valeur 1", "Valeur 2", "Valeur 3" });
        combo.setValueList((Object[]) new String[] { "0", "1", "2", "3" });

        TangoKey attributekey = new TangoKey();
        TangoKeyTool.registerAttribute(attributekey, "tango/tangotest/titan", "ampli");
        stringBox.connectWidget(textField2, attributekey);

        matrixBox.connectWidget(table, simulatedKey.getImageNumberMatrixKey());

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Comete V2 SimplePanel");
        SimplePanel simplePanel = new SimplePanel();
        simplePanel.connect();
        frame.setSize(400, 400);
        frame.setContentPane(simplePanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }
}
