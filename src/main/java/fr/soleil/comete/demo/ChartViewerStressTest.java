package fr.soleil.comete.demo;

import java.text.NumberFormat;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

import fr.soleil.comete.swing.Chart;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.swing.dialog.ProgressDialog;

public class ChartViewerStressTest {

    public static void main(String[] args) {
        final Chart chartViewer = new Chart();
        chartViewer.setManagementPanelVisible(true);
        chartViewer.setAxisSelectionVisible(true);
        chartViewer.setAutoHighlightOnLegend(true);
        final JFrame testFrame = new JFrame(Chart.class.getSimpleName() + " test");
        testFrame.setContentPane(chartViewer);
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setSize(600, 600);
        testFrame.setVisible(true);

        ICancelable killer = new ICancelable() {
            @Override
            public void setCanceled(boolean canceled) {
                if (canceled) {
                    System.exit(0);
                }
            }

            @Override
            public boolean isCanceled() {
                return false;
            }
        };

        final ProgressDialog progressDialog = new ProgressDialog(testFrame);
        progressDialog.setProgressIndeterminate(true);
        progressDialog.setCancelable(killer);
        progressDialog.setTitle("Computing chart data");
        progressDialog.setMainMessage(progressDialog.getTitle());
        progressDialog.pack();
        progressDialog.setLocationRelativeTo(testFrame);
        progressDialog.setVisible(true);

        SwingWorker<Void, Void> chartFiller = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                Map<String, Object> data = new LinkedHashMap<String, Object>();
                String tmpName = null;
                final NumberFormat format = NumberFormat.getInstance();
                format.setMinimumIntegerDigits(3);
                // Create 1500 dataview of 650 points
                for (int i = 0; i < 1500; i++) {
                    tmpName = "Curve-" + format.format(i);
                    double[] flatValues = new double[650];
                    for (int j = 0; j < flatValues.length / 2; j++) {
                        flatValues[2 * j] = 2 * j;
                        flatValues[2 * j + 1] = 2 * j + 1 + i;
                    }
                    data.put(tmpName, flatValues);
                }
                chartViewer.setData(data);
                return null;
            }

            @Override
            protected void done() {
                progressDialog.setVisible(false);
            }
        };
        chartFiller.execute();
    }
}
