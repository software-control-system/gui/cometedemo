package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.util.prefs.Preferences;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.swing.Label;

public class TangoBoxTest extends AbstractTangoBox {

    private static final long serialVersionUID = 4041911786075475939L;
    private Label ampliLabel = new Label();
    private Label doubleScalar = new Label();

    public TangoBoxTest() {
        super();
        initDisplayedComponents();
    }

    /*
     * Initializes the displayed components/widgets
     */
    private void initDisplayedComponents() {
        // getStatusPanel().setBorder(new LineBorder(Color.GRAY));
        setLayout(new BorderLayout());
        add(getStateLabel(), BorderLayout.NORTH);
        JPanel scalarPanel = new JPanel();
        scalarPanel.add(ampliLabel);
        scalarPanel.add(doubleScalar);
        add(scalarPanel, BorderLayout.CENTER);
        add(getStatusPanel(), BorderLayout.SOUTH);
    }

    @Override
    protected void refreshGUI() {
        if (getModel() != null && !getModel().isEmpty()) {
            setStatusModel();
            setWidgetModel(ampliLabel, stringBox, generateAttributeKey("ampli"));
            setWidgetModel(doubleScalar, stringBox, generateWriteAttributeKey("double_scalar"));

        }

    }

    @Override
    protected void clearGUI() {

        // cleanStatusModel();
        cleanWidget(getStateLabel());
        cleanWidget(getDeviceStatusLabel());
        cleanWidget(ampliLabel);
        cleanWidget(doubleScalar);
    }

    @Override
    protected void onConnectionError() {
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // nothing to do
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // nothing to do
    }

    public static void main(String[] args) {
        final JFrame mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        String deviceName = "tango/tangotest/1";
        JTabbedPane panel = new JTabbedPane();
        TangoBoxTest bean1 = new TangoBoxTest();
        bean1.setModel(deviceName);
        TangoBoxTest bean2 = new TangoBoxTest();
        bean2.setModel(deviceName);

        panel.add("bean1", bean1);
        panel.add("bean2", bean2);

        mainFrame.setContentPane(panel);
        mainFrame.setTitle(TangoBoxTest.class.getSimpleName());
        bean1.stop();
        bean2.stop();
        bean1.start();
        bean2.start();
        bean1.stop();
        bean1.start();
        bean2.start();

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                mainFrame.pack();
                mainFrame.setLocationRelativeTo(null);
                mainFrame.setVisible(true);
            }
        });
    }

}
