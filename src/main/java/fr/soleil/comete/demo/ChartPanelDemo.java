package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.chart.util.ShowInFrameMenu;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.TangoPolledRefreshingStrategy;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IKey;

public class ChartPanelDemo extends JPanel {

    private static final long serialVersionUID = 9187746115121871326L;

    public static final String DEFAULT_TANGO_LABEL = "Tango";
    public static final String DEFAULT_NEXUS_LABEL = "Nexus";

    private final Map<String, IKey> keyMap = new HashMap<String, IKey>();
    private int selectedAxis = IChartViewer.Y1;

    private final JTabbedPane tabbedPane;
    private final JPanel sourcePanel;

    private final Chart chartViewer;
    private final ChartViewerBox chartBox;

    private final ImageViewer imageViewer;
    private final NumberMatrixBox imageBox;

    public ChartPanelDemo() {
        super(new BorderLayout());

        sourcePanel = initSourcePanel();

        chartViewer = new Chart();
        ShowInFrameMenu showInFrameMenu = new ShowInFrameMenu();
        chartViewer.addMenuItem(showInFrameMenu);
        String[] labels = { "Test1", "Test2", "Test3" };
        double[] labelPositions = { 0.0, 10.0, 20.0 };
        chartViewer.setLabels(IChartViewer.X, labels, labelPositions);
        // chartViewer.addChartViewerListener(this);
        chartViewer.setAutoHighlightOnLegend(true);
        chartBox = new ChartViewerBox();

        imageViewer = new ImageViewer();
        imageViewer.setShowRoiInformationTable(true);
        imageBox = new NumberMatrixBox();

        tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Chart Viewer", chartViewer);
        tabbedPane.addTab("Image Viewer", imageViewer);

        add(tabbedPane, BorderLayout.CENTER);
        add(sourcePanel, BorderLayout.SOUTH);
    }

    private JPanel initSourcePanel() {
        final JPanel panel = new JPanel(new BorderLayout());

        final JComboBox<String> sourceCombo = new JComboBox<>();
        sourceCombo.addItem(DEFAULT_TANGO_LABEL);
        sourceCombo.addItem(DEFAULT_NEXUS_LABEL);
        sourceCombo.setPreferredSize(new Dimension(100, 50));

        final TextField sourceParameter = new TextField();
        sourceParameter.setText("tangotest1/wave");

        final Button addSource = new Button("Add Source");
        addSource.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addNextSource((String) sourceCombo.getSelectedItem(), sourceParameter.getText());
            }
        });

        final Button cleanSource = new Button("Clean");
        cleanSource.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cleanSources();
            }
        });

        final JComboBox<String> axisCombo = new JComboBox<>();
        axisCombo.addItem("Y1");
        axisCombo.addItem("Y2");

        axisCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                selectedAxis = IChartViewer.Y1;
                if (axisCombo.getSelectedIndex() == 1) {
                    selectedAxis = IChartViewer.Y2;
                }
                changeAxis(sourceParameter.getText());
            }
        });

        panel.add(sourceCombo, BorderLayout.WEST);
        panel.add(sourceParameter, BorderLayout.CENTER);
        final JPanel buttonPanel = new JPanel();
        buttonPanel.add(addSource);
        buttonPanel.add(cleanSource);
        buttonPanel.add(axisCombo);
        panel.add(buttonPanel, BorderLayout.EAST);

        return panel;
    }

    private void addNextSource(String currentSource, String parameter) {
        if ((parameter != null) && !parameter.isEmpty()) {
            if (currentSource.equals(DEFAULT_TANGO_LABEL)) {
                connectNewTangoAttribute(parameter);
            }
            if (currentSource.equals(DEFAULT_NEXUS_LABEL)) {
                connectNewNexusFile(parameter);
            }
        }
    }

    private void connectNewNexusFile(String nexusFile) {
        // TODO update code
        // if (nexusFile != null && nexusFile.contains(";")) {
        // CDMAKey key = new CDMAKey();
        // key.registerFactoryId(NcFactory.NAME);
        // String[] nexusInfo = nexusFile.split(";");
        // String filePath = nexusInfo[0];
        // String dataPath = nexusInfo[1];
        //
        // Component tab = tabbedPane.getSelectedComponent();
        // if (tab.equals(chartViewer)) {
        // key.registerSpectrumData(filePath, dataPath);
        // }
        // else if (tab.equals(imageViewer)) {
        // key.registerImageData(filePath, dataPath);
        // }
        // connectToSelectedPane(key);
        // }
    }

    private void connectNewTangoAttribute(String tangoName) {

        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, tangoName);
        keyMap.put(tangoName, key);
        connectToSelectedPane(key);
    }

    private void connectToSelectedPane(IKey key) {

        Component tab = tabbedPane.getSelectedComponent();
        if (tab.equals(chartViewer)) {
            chartBox.connectWidget(chartViewer, key);
            chartViewer.setDataViewAxis(key.getInformationKey(), selectedAxis);
            chartViewer.setDataViewDisplayName(key.getInformationKey(), "My_" + key.getInformationKey());
        } else if (tab.equals(imageViewer)) {
            imageBox.connectWidget(imageViewer, key);
            imageViewer.setShowRoiInformationTable(true);
        }
    }

    private void changeAxis(final String tangoName) {
        IKey key = keyMap.get(tangoName);
        if (key != null) {
            chartViewer.setDataViewAxis(key.getInformationKey(), selectedAxis);
        }
    }

    private void cleanSources() {
        Component tab = tabbedPane.getSelectedComponent();
        if (tab.equals(chartViewer)) {
            chartBox.disconnectWidgetFromAll(chartViewer);
            chartViewer.resetAll();
        } else if (tab.equals(imageViewer)) {
            imageBox.disconnectWidgetFromAll(imageViewer);
            imageViewer.setFlatNumberMatrix(null, 0, 0);
        }
        keyMap.clear();
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        DataSourceProducerProvider.pushNewProducer(TangoDataSourceFactory.class);
        TangoDataSourceFactory tango = (TangoDataSourceFactory) DataSourceProducerProvider
                .getProducer(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
        tango.setDefaultRefreshingStrategy(new TangoPolledRefreshingStrategy(10000));
        ChartPanelDemo panel = new ChartPanelDemo();

        panel.setSize(600, 600);
        panel.setPreferredSize(panel.getSize());
        panel.setEnabled(true);

        frame.setContentPane(panel);
        frame.setSize(600, 600);
        frame.setTitle(panel.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}
