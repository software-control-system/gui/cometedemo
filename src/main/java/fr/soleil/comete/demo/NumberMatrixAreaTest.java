package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.matrixbox.BooleanMatrixBox;
import fr.soleil.comete.definition.listener.ITableListener;
import fr.soleil.comete.swing.NumberMatrixArea;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.container.matrix.AbstractMatrix;

public class NumberMatrixAreaTest {

//    private final static NumberMatrixBox numberMatrixBox = new NumberMatrixBox();
    private final static BooleanMatrixBox booleanMatrixBox = new BooleanMatrixBox();
    private static final String DEVICENAME = "tango/tangotest/1";
    private static NumberMatrixArea numberMatrixArea;
    private static String spectrum = "double_spectrum";
    private static String image = "double_image";
    private static String booleann = "boolean_image";

    private static void connectWidget(String attributeName) {
        System.out.println("******************connectWidget(" + attributeName + ")");
        booleanMatrixBox.disconnectWidgetFromAll(numberMatrixArea);
        TangoKey key = new TangoKey();
        TangoKeyTool.registerWriteAttribute(key, DEVICENAME, attributeName);
        booleanMatrixBox.connectWidget(numberMatrixArea, key);
        // booleanMatrixBox.connectWidget(numberMatrixArea, key);
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame();
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        numberMatrixArea = new NumberMatrixArea();
        mainPanel.add(numberMatrixArea, BorderLayout.CENTER);

        final JComboBox<String> jComboBox = new JComboBox<>(new String[] { spectrum, image, booleann });
        mainPanel.add(jComboBox, BorderLayout.SOUTH);

        jComboBox.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String attributeName = jComboBox.getSelectedItem().toString();
                connectWidget(attributeName);
            }
        });
        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setTitle("NumberMatrixAreaTest");
        frame.setAlwaysOnTop(true);
        frame.setSize(500, 500);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.toFront();
        ITableListener<AbstractMatrix<?>> iTableListener = new ITableListener<AbstractMatrix<?>>() {

            @Override
            public void valueChanged(AbstractMatrix<?> matrix) {

            }

            @Override
            public void selectedPointChanged(double[] point) {

            }

            @Override
            public void selectedColumnChanged(int col) {

            }

            @Override
            public void selectedRowChanged(int row) {

            }

        };
        numberMatrixArea.addTableListener(iTableListener);
        // connectWidget(spectrum);
        connectWidget(booleann);

    }

}
