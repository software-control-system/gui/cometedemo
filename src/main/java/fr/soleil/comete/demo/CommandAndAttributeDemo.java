package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.demo.testbox.StringButtonTest;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

// Tests JAVAAPI-253 case
public class CommandAndAttributeDemo {

    private static final String NO_COMMAND = "No command";
    private static final String NO_ATTRIBUTE = "No attribute";
    private static final String NO_ENTITY = "No entity";
    private static final String ATTRIBUTE = "attribute";
    private static final String COMMAND = "command";
    private static final TitledBorder CONNECTED_COMMAND_BORDER = new TitledBorder("Connected command");
    private static final TitledBorder CONNECTED_ATTRIBUTE_BORDER = new TitledBorder("Connected attribute");
    private static final TitledBorder CONNECTED_ENTITY_BORDER = new TitledBorder("Connected entity");

    public static void main(String[] args) {
        final JTextField deviceNameField = new JTextField("tango/tangotest/titan", 20);
        deviceNameField.setBorder(new TitledBorder("Device name"));
        final JTextField entityNameField = new JTextField("init", 20);
        entityNameField.setBorder(new TitledBorder("Entity name"));
        final StringButton commandButton = new StringButton();
        commandButton.setText(NO_COMMAND);
        final Label attributeLabel = new Label();
        attributeLabel.setText(NO_ATTRIBUTE);
        final CardLayout entityLayout = new CardLayout();
        final JPanel entityPanel = new JPanel(entityLayout);
        entityPanel.setBorder(CONNECTED_ENTITY_BORDER);
        JButton connectCommandButton = new JButton("connect command");
        JButton connectAttributeButton = new JButton("connect attribute");
        final JLabel noEntityLabel = new JLabel(NO_ENTITY, JLabel.CENTER);
        entityPanel.add(commandButton, COMMAND);
        entityPanel.add(attributeLabel, ATTRIBUTE);
        entityPanel.add(noEntityLabel, NO_ENTITY);
        entityLayout.show(entityPanel, NO_ENTITY);
        final StringScalarBox box = new StringScalarBox();
        connectCommandButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String command = entityNameField.getText().trim();
                commandButton.setText(command.isEmpty() ? NO_COMMAND : command);
                entityLayout.show(entityPanel, COMMAND);
                entityPanel.setBorder(CONNECTED_COMMAND_BORDER);

//                box.setUserEnabled(commandButton, false);

                // Step 1: disconnect widgets
                box.disconnectWidgetFromAll(commandButton);
                box.disconnectWidgetFromAll(attributeLabel);

                // Step 2: create key
                TangoKey key = new TangoKey();
                TangoKeyTool.registerCommand(key, deviceNameField.getText().trim(), command);

                // Step 3: connect widget
                box.connectWidget(commandButton, key);
            }

        });
        connectAttributeButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String attribute = entityNameField.getText().trim();
                attributeLabel.setText(attribute.isEmpty() ? NO_ATTRIBUTE : attribute);
                entityLayout.show(entityPanel, ATTRIBUTE);
                entityPanel.setBorder(CONNECTED_ATTRIBUTE_BORDER);

//                box.setUserEnabled(entityNameField, false);

                // Step 1: disconnect widgets
                box.disconnectWidgetFromAll(attributeLabel);
                box.disconnectWidgetFromAll(commandButton);

                // Step 2: create key
                TangoKey key = new TangoKey();
                TangoKeyTool.registerAttribute(key, deviceNameField.getText().trim(), attribute);

                // Step 3: connect widget
                box.connectWidget(attributeLabel, key);
            }

        });

        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints deviceNameFieldConstraints = new GridBagConstraints();
        deviceNameFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        deviceNameFieldConstraints.gridx = 0;
        deviceNameFieldConstraints.gridy = 0;
        deviceNameFieldConstraints.weightx = 1;
        deviceNameFieldConstraints.weighty = 0;
        deviceNameFieldConstraints.insets = new Insets(0, 0, 0, 0);
        mainPanel.add(deviceNameField, deviceNameFieldConstraints);

        GridBagConstraints entityNameFieldConstraints = new GridBagConstraints();
        entityNameFieldConstraints.fill = GridBagConstraints.HORIZONTAL;
        entityNameFieldConstraints.gridx = 0;
        entityNameFieldConstraints.gridy = 1;
        entityNameFieldConstraints.weightx = 1;
        entityNameFieldConstraints.weighty = 0;
        entityNameFieldConstraints.insets = new Insets(0, 0, 0, 0);
        mainPanel.add(entityNameField, entityNameFieldConstraints);

        GridBagConstraints connectCommandButtonConstraints = new GridBagConstraints();
        connectCommandButtonConstraints.fill = GridBagConstraints.NONE;
        connectCommandButtonConstraints.gridx = 1;
        connectCommandButtonConstraints.gridy = 0;
        connectCommandButtonConstraints.weightx = 0;
        connectCommandButtonConstraints.weighty = 0;
        connectCommandButtonConstraints.gridheight = 2;
        connectCommandButtonConstraints.insets = new Insets(0, 10, 0, 0);
        mainPanel.add(connectCommandButton, connectCommandButtonConstraints);

        GridBagConstraints connectAttributeButtonConstraints = new GridBagConstraints();
        connectAttributeButtonConstraints.fill = GridBagConstraints.NONE;
        connectAttributeButtonConstraints.gridx = 2;
        connectAttributeButtonConstraints.gridy = 0;
        connectAttributeButtonConstraints.weightx = 0;
        connectAttributeButtonConstraints.weighty = 0;
        connectAttributeButtonConstraints.gridheight = 2;
        connectAttributeButtonConstraints.insets = new Insets(0, 10, 0, 0);
        mainPanel.add(connectAttributeButton, connectAttributeButtonConstraints);

        GridBagConstraints entityPanelConstraints = new GridBagConstraints();
        entityPanelConstraints.fill = GridBagConstraints.HORIZONTAL;
        entityPanelConstraints.gridx = 0;
        entityPanelConstraints.gridy = 2;
        entityPanelConstraints.weightx = 0;
        entityPanelConstraints.weighty = 0;
        entityPanelConstraints.gridwidth = GridBagConstraints.REMAINDER;
        entityPanelConstraints.insets = new Insets(10, 0, 0, 0);
        mainPanel.add(entityPanel, entityPanelConstraints);

        JFrame frame = new JFrame(StringButtonTest.class.getSimpleName() + " " + deviceNameField.getText() + "/"
                + entityNameField.getText());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(mainPanel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }

}
