package fr.soleil.comete.demo;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import fr.soleil.comete.swing.Chart;

public class ChartTimeTest {

    public static void main(String[] args) {
        Chart viewer = new Chart();
        // viewer.setAnnotation(IChartViewer.TIME_ANNO, IChartViewer.X);
        long time = System.currentTimeMillis();
        long before = time - 2L;
        Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
        for (int i = 0; i < 5; i++) {
            double[] data = new double[4];
            data[0] = before;
            data[1] = Math.random() * 100;
            data[2] = time;
            data[3] = Math.random() * 100;
            dataMap.put("Data " + (i + 1), data);
        }
        viewer.setData(dataMap);
        JFrame testFrame = new JFrame(ChartTimeTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(viewer);
        testFrame.setSize(400, 300);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }
}
