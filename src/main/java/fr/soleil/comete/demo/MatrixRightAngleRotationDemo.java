package fr.soleil.comete.demo;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.definition.util.DimensionReverter;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.data.container.IDataContainer;
import fr.soleil.data.filter.MatrixRightAngleRotation;
import fr.soleil.data.filter.MatrixSymmetry;
import fr.soleil.data.service.GenericDescriptor;

public class MatrixRightAngleRotationDemo {

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        int width = 3, height = 2;
        if ((args != null) && (args.length > 1)) {
            try {
                width = Integer.parseInt(args[0]);
                height = Integer.parseInt(args[1]);
            } catch (Exception e) {
                width = 3;
                height = 2;
            }
        }
        JPanel mainPanel = new JPanel(new GridLayout(3, 2));
        ImageViewer viewer = new ImageViewer();
        viewer.setApplicationId(MatrixRightAngleRotationDemo.class.getSimpleName());
        viewer.setAlwaysFitMaxSize(true);
        viewer.setImageName("Normal data");
        ImageViewer positiveRotated = new ImageViewer();
        positiveRotated.setApplicationId(MatrixRightAngleRotationDemo.class.getSimpleName());
        positiveRotated.setAlwaysFitMaxSize(true);
        positiveRotated.setImageName("Positive Rotated data");
        ImageViewer negativeRotated = new ImageViewer();
        negativeRotated.setApplicationId(MatrixRightAngleRotationDemo.class.getSimpleName());
        negativeRotated.setAlwaysFitMaxSize(true);
        negativeRotated.setImageName("Negative Rotated data");
        ImageViewer rotated180 = new ImageViewer();
        rotated180.setApplicationId(MatrixRightAngleRotationDemo.class.getSimpleName());
        rotated180.setAlwaysFitMaxSize(true);
        rotated180.setImageName("180 Rotated data");
        ImageViewer symH = new ImageViewer();
        symH.setApplicationId(MatrixRightAngleRotationDemo.class.getSimpleName());
        symH.setAlwaysFitMaxSize(true);
        symH.setImageName("Symmetry, Horizontal axis");
        ImageViewer symV = new ImageViewer();
        symV.setApplicationId(MatrixRightAngleRotationDemo.class.getSimpleName());
        symV.setAlwaysFitMaxSize(true);
        symV.setImageName("Symmetry, Vertical axis");
        mainPanel.add(viewer);
        mainPanel.add(rotated180);
        mainPanel.add(positiveRotated);
        mainPanel.add(negativeRotated);
        mainPanel.add(symH);
        mainPanel.add(symV);
        final Number[][] data = new Number[height][width];
        int value = 1;
        for (Number[] row : data) {
            for (int i = 0; i < row.length; i++) {
                row[i] = value++;
            }
        }
        IDataContainer<Object[]> fakeContainer = new IDataContainer<Object[]>() {
            @Override
            public void setData(Object[] data) {
                // nothing to do
            }

            @Override
            public boolean isSettable() {
                // nothing to do
                return false;
            }

            @Override
            public GenericDescriptor getDataType() {
                // nothing to do
                return null;
            }

            @Override
            public Number[][] getData() throws Exception {
                // nothing to do
                return data;
            }

            @Override
            public boolean isUnsigned() {
                // nothing to do
                return false;
            }
        };
        MatrixRightAngleRotation positiveRotation = new MatrixRightAngleRotation(fakeContainer, true);
        MatrixRightAngleRotation negativeRotation = new MatrixRightAngleRotation(fakeContainer, false);
        MatrixSymmetry symmetry = new MatrixSymmetry(new MatrixSymmetry(fakeContainer, MatrixSymmetry.HORIZONTAL),
                MatrixSymmetry.VERTICAL);
        MatrixSymmetry symmetryH = new MatrixSymmetry(fakeContainer, MatrixSymmetry.HORIZONTAL);
        MatrixSymmetry symmetryV = new MatrixSymmetry(fakeContainer, MatrixSymmetry.VERTICAL);
        viewer.setNumberMatrix(data);
        positiveRotated.setNumberMatrix(positiveRotation.getData());
        positiveRotated.setYAxisConvertor(new DimensionReverter(data[0].length));
        negativeRotated.setNumberMatrix(negativeRotation.getData());
        negativeRotated.setXAxisConvertor(new DimensionReverter(data.length));
        rotated180.setNumberMatrix(symmetry.getData());
        rotated180.setYAxisConvertor(new DimensionReverter(data.length));
        rotated180.setXAxisConvertor(new DimensionReverter(data[0].length));
        symH.setNumberMatrix(symmetryH.getData());
        symH.setYAxisConvertor(new DimensionReverter(data.length));
        symV.setNumberMatrix(symmetryV.getData());
        symV.setXAxisConvertor(new DimensionReverter(data[0].length));
        JFrame testFrame = new JFrame("MatrixRightAngleRotationTest");
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(mainPanel);
        testFrame.setSize(750, 550);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }
}
