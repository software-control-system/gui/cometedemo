package fr.soleil.comete.demo;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.date.IDateFormattable;

public class DualChartTest {
    // TANGO TEST PARAMETERS
    private final static String DEVICE = "tango/tangotest/1";

    private final ChartViewerBox chartBox = new ChartViewerBox();

    // END OF TANGO PARAMETERS

    public DualChartTest() {

    }

    public JPanel initPanel() {
        JPanel result = new JPanel(new BorderLayout());
        final Chart viewer = new Chart();
        viewer.setAutoHighlightOnLegend(true);
        viewer.setDataViewsSortedOnX(false);
        connectViewer(viewer);
        result.add(viewer, BorderLayout.CENTER);
        writeData();
        return result;
    }

    private void writeData() {
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(DEVICE);

        if (proxy != null) {
            try {
                DeviceAttribute deviceAttribute = proxy.read_attribute("double_spectrum");
                double[] value1 = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                deviceAttribute.insert(value1);
                proxy.write_attribute(deviceAttribute);

                deviceAttribute = proxy.read_attribute("float_spectrum");
                float[] value2 = new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                deviceAttribute.insert(value2);
                proxy.write_attribute(deviceAttribute);

                deviceAttribute = proxy.read_attribute("long_spectrum");
                int[] value3 = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                deviceAttribute.insert(value3);
                proxy.write_attribute(deviceAttribute);

                deviceAttribute = proxy.read_attribute("double_image");
                double[] value4 = new double[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
                deviceAttribute.insert(value4, 10, 2);
                proxy.write_attribute(deviceAttribute);

            } catch (DevFailed e) {
                e.printStackTrace();
            }
        }
    }

    private void connectViewer(final IChartViewer viewer) {
        TangoKey keyX = new TangoKey();
        TangoKeyTool.registerAttribute(keyX, DEVICE, "double_spectrum");

        TangoKey keyY1 = new TangoKey();
        TangoKeyTool.registerAttribute(keyY1, DEVICE, "float_spectrum");

        TangoKey keyY2 = new TangoKey();
        TangoKeyTool.registerAttribute(keyY2, DEVICE, "long_spectrum");

        TangoKey keyY3 = new TangoKey();
        TangoKeyTool.registerAttribute(keyY3, DEVICE, "double_image");

        TangoKey keyY4 = new TangoKey();
        TangoKeyTool.registerAttribute(keyY4, "ICA/SALSA/SCAN.2", "actuator_1_1");

        TangoKey keyY5 = new TangoKey();
        TangoKeyTool.registerAttribute(keyY5, "ICA/SALSA/SCAN.2", "actuator_2_1");

        TangoKey keyY6 = new TangoKey();
        TangoKeyTool.registerAttribute(keyY6, "ICA/SALSA/SCAN.1", "data_01");
        chartBox.connectWidget(viewer, keyY6);

        TangoKey keyY7 = new TangoKey();
        TangoKeyTool.registerAttribute(keyY7, "ICA/SALSA/SCAN.1", "sensorsTimestamps");

        chartBox.connectWidgetDual(viewer, keyY7, keyY6);
        viewer.setAxisLabelFormat(IChartViewer.TIME_FORMAT, IChartViewer.X);
        viewer.setAxisDateFormat(IDateFormattable.US_DATE_FORMAT, IChartViewer.X);
        // viewer.setAnnotation(IChartViewer.TIME_ANNO, IChartViewer.X);

        // chartBox.connectWidgetDual(viewer, keyX, keyY1);
        //
        // chartBox.connectWidgetDual(viewer, keyX, keyY2);
        //
        // chartBox.connectWidgetDual(viewer, keyX, keyY3);
        // chartBox.connectWidget(viewer, keyY3);
        // chartBox.connectWidget(viewer, keyY4);

        IKey dualKey = ChartViewerBox.createDualKey(keyY4, keyY5);
        chartBox.connectWidget(viewer, dualKey);
        chartBox.setSplitMatrixThroughColumns(dualKey, true);
        viewer.setDataViewMarkerStyle(keyY5.getInformationKey(), IChartViewer.MARKER_BOX);

    }

    private String getDeviceName() {
        return DualChartTest.DEVICE;
    }

    public static void main(String[] args) {
        System.setProperty("TANGO_HOST", "calypso:20001");

        JFrame frame = new JFrame();
        DualChartTest tester = new DualChartTest();

        frame.setContentPane(tester.initPanel());
        frame.setSize(900, 800);
        frame.setTitle("Testing COMETE on " + tester.getDeviceName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}
