package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class ImageBoxTest extends JPanel {

    private static final long serialVersionUID = -1488388801100084358L;

    private final ImageViewer imageViewer = new ImageViewer();
    private final NumberMatrixBox imageBox = new NumberMatrixBox();
    private final JTextField attributeField = new JTextField();
    private TangoKey tangoKey = null;

    public ImageBoxTest() {
        super(new BorderLayout());
        imageViewer.setApplicationId("ID");
        add(imageViewer, BorderLayout.CENTER);
        JPanel buttonPanel = new JPanel();
        JButton connectButton = new JButton("Connect");
        JButton disconnectButton = new JButton("Disconnect");
        buttonPanel.add(connectButton);
        buttonPanel.add(disconnectButton);
        add(buttonPanel, BorderLayout.SOUTH);
        add(attributeField, BorderLayout.NORTH);
        attributeField.setText("tango/tangotest/1/double_image_ro");

        connectButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                tangoKey = new TangoKey();
                TangoKeyTool.registerAttribute(tangoKey, attributeField.getText().trim());
                connect();
            }
        });

        disconnectButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                disconnect();
            }
        });
    }

    private void connect() {
        long start = System.currentTimeMillis();
        imageBox.connectWidget(imageViewer, tangoKey);
        long end = System.currentTimeMillis();
        System.out.println("Connect time = " + (end - start));
    }

    private void disconnect() {
        imageBox.disconnectWidget(imageViewer, tangoKey);
    }

    public static void main(String[] args) {
        System.setProperty("TANGO_HOST", "tangodb:20001");
        ImageBoxTest image = new ImageBoxTest();
        JFrame testFrame = new JFrame(ImageBoxTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(image);
        testFrame.setSize(600, 700);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }
}
