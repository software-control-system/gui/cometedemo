package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.Slider;
import fr.soleil.comete.swing.Spinner;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class NumberFieldTest {

    private static JPanel initPanel() {

        final JPanel panel = new JPanel(new BorderLayout());
        NumberScalarBox numberBox = new NumberScalarBox();

        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "ampli");

        TangoKey secondkey = new TangoKey();
        TangoKeyTool.registerAttribute(secondkey, "tango/tangotest/titan", "double_scalar");

        Spinner spinner = new Spinner();
        WheelSwitch wheelswitch = new WheelSwitch();
        wheelswitch.setInvertionLogic(true);
        Slider slider = new Slider();

        numberBox.connectWidget(spinner, secondkey);
        numberBox.connectWidget(wheelswitch, key);
        numberBox.connectWidget(slider, key);

        numberBox.setConfirmation(spinner, true);

        panel.add(spinner, BorderLayout.CENTER);
        panel.add(wheelswitch, BorderLayout.EAST);
        panel.add(slider, BorderLayout.NORTH);

        final JLabel label = new JLabel("Click here to loose focus");

        panel.add(label, BorderLayout.SOUTH);
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                label.grabFocus();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });

        return panel;
    }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        final JPanel f = initPanel();
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("NumberField Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        f.grabFocus();
    }
}
