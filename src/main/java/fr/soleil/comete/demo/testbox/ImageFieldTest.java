package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class ImageFieldTest {

    private static JPanel initPanel(IImageViewer imageViewer) {
        final JPanel panel = new JPanel(new BorderLayout());

        if (imageViewer instanceof JComponent) {
            panel.add((JComponent) imageViewer, BorderLayout.CENTER);
        }

        final JLabel label = new JLabel("Click here to loose focus");

        panel.add(label, BorderLayout.SOUTH);
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                label.grabFocus();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });

        return panel;
    }

    private static IImageViewer createImageViewer() {
        ImageViewer viewer = new ImageViewer();
        viewer.setCleanOnDataSetting(false);

        NumberMatrixBox imageBox = new NumberMatrixBox();

        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_image_ro");

        imageBox.connectWidget(viewer, key);

        return viewer;
    }

    // private static IImageViewer createImagePlayer() {
    //
    // IImageViewer viewer = new ImagePlayer();
    //
    // ImageViewerBox imageBox = new ImageViewerBox();
    //
    // TangoKey key = new TangoKey();
    // TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_image_ro");
    //
    // imageBox.connectWidget(viewer, key);
    //
    // return viewer;
    // }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        IImageViewer image = createImageViewer();

        final JPanel f = initPanel(image);
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("NumberField Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}
