package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.Slider;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class SliderTest {

    private static JPanel initPanel() {

        final JPanel panel = new JPanel(new BorderLayout());
        NumberScalarBox numberBox = new NumberScalarBox();
        StringScalarBox stringBox = new StringScalarBox();

        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "short_scalar_ro");
        Label label = new Label();
        label.setHorizontalAlignment(SwingConstants.CENTER);

        Slider slider = new Slider();
        slider.setPreferedMajorTickColor(Color.BLACK);
        slider.setPreferedMinorTickColor(Color.LIGHT_GRAY);
        slider.setPaintTicks(true);
        numberBox.setDefaultToolTipEnabled(false);
        numberBox.connectWidget(slider, key);
        stringBox.connectWidget(label, key);

        slider.setStep(0.2);
        slider.setMinimumDouble(-20);
        slider.setMaximumDouble(120);
        slider.setPaintLabels(true);

        panel.add(slider, BorderLayout.CENTER);
        panel.add(label, BorderLayout.SOUTH);

        return panel;
    }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        final JPanel f = initPanel();
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("Slider Test");
        frame.setContentPane(f);
        frame.setSize(1000, 120);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        f.grabFocus();
    }
}
