package fr.soleil.comete.demo.testbox;

import javax.swing.JPanel;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class LabelTest {

    private static JPanel initPanel() {
        final JPanel panel = new JPanel();

        StringScalarBox stringBox = new StringScalarBox();

        TangoKey stringScalarKey = new TangoKey();
        TangoKeyTool.registerAttribute(stringScalarKey, "tango/tangotest/titan", "string_scalar");

        TangoKey stateKey = new TangoKey();
        TangoKeyTool.registerAttribute(stateKey, "tango/tangotest/titan", "State");

        TangoKey numberScalarKey = new TangoKey();
        TangoKeyTool.registerAttribute(numberScalarKey, "tango/tangotest/titan", "double_scalar");

        TangoKey numberScalarKey2 = new TangoKey();
        TangoKeyTool.registerAttribute(numberScalarKey2, "tango/tangotest/titaN", "double_scalar");

        TangoKey motorPositionKey = new TangoKey();
        TangoKeyTool.registerAttribute(motorPositionKey, "samba/mono/b1", "offset");

        Label stringLabel = new Label();
        Label stateLabel = new Label();
        Label numberLabel = new Label();
        Label numberLabel2 = new Label();
        Label motorLabel = new Label();

        stringBox.connectWidget(stringLabel, stringScalarKey);
        stringBox.connectWidget(stateLabel, stateKey);
        stringBox.connectWidget(numberLabel, numberScalarKey);
        stringBox.setUnitEnabled(numberLabel, false);
        stringBox.connectWidget(numberLabel2, numberScalarKey2);
        // stringBox.setFormatEnabled(numberLabel, true);
        // stringBox.setFormatEnabled(numberLabel2, true);
        // stringBox.setSettable(numberLabel, false);
        stringBox.connectWidget(motorLabel, motorPositionKey);

        panel.add(stringLabel);
        panel.add(stateLabel);
        panel.add(numberLabel);
        panel.add(numberLabel2);
        panel.add(motorLabel);

        return panel;
    }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        final JPanel f = initPanel();
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("Label Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
