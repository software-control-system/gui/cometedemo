package fr.soleil.comete.demo.testbox;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class BooleanBoxTest {

    private static JPanel initPanel() {
        final JPanel panel = new JPanel();

        BooleanScalarBox booleanScalarBox = new BooleanScalarBox();

        TangoKey booleanScalarRWKey = new TangoKey();
        TangoKeyTool.registerAttribute(booleanScalarRWKey, "tango/tangotest/1", "boolean_scalar");

        TangoKey booleanScalarROKey = new TangoKey();
        TangoKeyTool.registerAttribute(booleanScalarROKey, "storage/recorder/datarecorder.1", "nxentrypostrecording");

        CheckBox checkboxRW = new CheckBox();
        booleanScalarBox.connectWidget(checkboxRW, booleanScalarRWKey);

        CheckBox checkboxRO = new CheckBox();
        booleanScalarBox.connectWidget(checkboxRO, booleanScalarROKey);

        CheckBox disableCB = new CheckBox();
        booleanScalarBox.connectWidget(disableCB, booleanScalarROKey);
        disableCB.setEnabled(false);

        panel.add(new JLabel("boolean RW"));
        panel.add(checkboxRW);
        panel.add(new JLabel("boolean RO"));
        panel.add(checkboxRO);
        panel.add(new JLabel("Disable CB"));
        panel.add(disableCB);

        return panel;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();

        final JPanel f = initPanel();
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());

        frame.setTitle("Checkbox Test");
        frame.setContentPane(f);
        frame.pack();
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
