package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.awt.Label;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.image.exception.ApplicationIdException;
import fr.soleil.comete.swing.util.CometeUtils;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.event.GroupEvent;
import fr.soleil.data.listener.IRefreshingGroupListener;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.GroupRefreshingStrategy;
import fr.soleil.lib.project.application.logging.LogAppender;
import fr.soleil.lib.project.application.logging.LogViewer;
import fr.soleil.lib.project.awt.layout.VerticalFlowLayout;

public class GroupRefreshingTest extends JPanel implements IRefreshingGroupListener {

    private static final long serialVersionUID = 8464795819133619295L;

    protected static final String GROUP_1 = "FirstGroup";
    protected static final String GROUP_2 = "SecondGroup";

    protected static final int GROUP_1_PERIOD = 2000;
    protected static final int GROUP_2_PERIOD = 800;

    protected static final GroupRefreshingStrategy STARTEGY_1 = new GroupRefreshingStrategy(GROUP_1, GROUP_1_PERIOD);
    protected static final GroupRefreshingStrategy STARTEGY_2 = new GroupRefreshingStrategy(GROUP_2, GROUP_2_PERIOD);

    protected final BufferedLabel group1Field;
    protected final BufferedImageViewer group1ImageViewer;

    protected final BufferedLabel group2Field1;
    protected final BufferedLabel group2Field2;

    protected final StringScalarBox stringScalarBox;
    protected final NumberMatrixBox numberMatrixBox;

    protected final LogViewer logPanel;
    protected final LogAppender logAppender;

    private final Logger logger;

    public GroupRefreshingTest() {
        super(new BorderLayout());

        String id = CometeUtils.generateIdForClass(getClass());
        logger = LoggerFactory.getLogger(id);

        stringScalarBox = new StringScalarBox();
        numberMatrixBox = new NumberMatrixBox();

        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
        leftPanel.setBorder(new TitledBorder("Group 1"));

        group1Field = new BufferedLabel();
        group1ImageViewer = new BufferedImageViewer();
        group1ImageViewer.setAlwaysFitMaxSize(true);

        leftPanel.add(group1ImageViewer);
        leftPanel.add(group1Field);

        JPanel rightPanel = new JPanel(new VerticalFlowLayout());
        rightPanel.setBorder(new TitledBorder("Group 2"));
        group2Field1 = new BufferedLabel();
        group2Field2 = new BufferedLabel();
        rightPanel.add(group2Field1);
        rightPanel.add(group2Field2);

        logPanel = new LogViewer(id);
        logAppender = new LogAppender(logPanel);
        LogAppender.registerAppender(logger, logAppender);

        JSplitPane horizontalSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        horizontalSplit.setLeftComponent(leftPanel);
        horizontalSplit.setRightComponent(new JScrollPane(rightPanel));
        horizontalSplit.setResizeWeight(0.5);
        horizontalSplit.setDividerLocation(0.5);
        horizontalSplit.setOneTouchExpandable(true);

        JSplitPane verticalSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        verticalSplit.setTopComponent(horizontalSplit);
        verticalSplit.setBottomComponent(logPanel);
        verticalSplit.setResizeWeight(0.7);
        verticalSplit.setDividerLocation(0.7);
        verticalSplit.setOneTouchExpandable(true);

        add(verticalSplit, BorderLayout.CENTER);
    }

    public void connect() {
        // connect group 1
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_scalar");
        stringScalarBox.connectWidget(group1Field, key);
        DataSourceProducerProvider.setRefreshingStrategy(key, STARTEGY_1);
        key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_image_ro");
        numberMatrixBox.connectWidget(group1ImageViewer, key);
        DataSourceProducerProvider.setRefreshingStrategy(key, STARTEGY_1);
        // connect group 2
        key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "long_scalar");
        stringScalarBox.connectWidget(group2Field1, key);
        DataSourceProducerProvider.setRefreshingStrategy(key, STARTEGY_2);
        key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "short_scalar_ro");
        stringScalarBox.connectWidget(group2Field2, key);
        DataSourceProducerProvider.setRefreshingStrategy(key, STARTEGY_2);

        DataSourceProducerProvider.getProducerByClass(TangoDataSourceFactory.class).addRefreshingGroupListener(this);
    }

    @Override
    public void groupRefreshed(GroupEvent event) {
        if (event != null) {
            if (TangoDataSourceFactory.META_INFORMATION_ATTRIBUTE.equals(event.getMetaInformation())) {
                if (GROUP_1.equals(event.getGroupName())) {
                    logger.info("Group 1 refreshed");
                    group1Field.transmit();
                    group1ImageViewer.transmit();
                } else if (GROUP_2.equals(event.getGroupName())) {
                    logger.info("Group 2 refreshed");
                    group2Field1.transmit();
                    group2Field2.transmit();
                }
            }
        }
    }

    public static void main(String[] args) {
        final GroupRefreshingTest groupRefreshingTest = new GroupRefreshingTest();
        JFrame testFrame = new JFrame(GroupRefreshingTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(groupRefreshingTest);
        testFrame.setSize(750, 550);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                groupRefreshingTest.connect();
            }
        });
    }

    // ///////////// //
    // Inner classes //
    // ///////////// //

    protected static interface ITrasnmittable {
        public void transmit();
    }

    protected class BufferedLabel extends Label implements ITrasnmittable {

        private static final long serialVersionUID = -8644492005225631018L;

        private volatile String bufferedText;

        @Override
        public void setText(String text) {
            this.bufferedText = text;
        }

        @Override
        public void transmit() {
            super.setText(bufferedText);
            revalidate();
            repaint();
        }
    }

    protected class BufferedImageViewer extends ImageViewer implements ITrasnmittable {

        private static final long serialVersionUID = 252727010836917909L;

        private volatile Object bufferedValue;
        private volatile int bufferedDimX;
        private volatile int bufferedDimY;

        @Override
        public void setFlatNumberMatrix(Object value, int width, int height) throws ApplicationIdException {
            bufferedValue = value;
            bufferedDimX = width;
            bufferedDimY = height;
        }

        @Override
        public void transmit() {
            super.setFlatNumberMatrix(bufferedValue, bufferedDimX, bufferedDimY);
        }
    }

}
