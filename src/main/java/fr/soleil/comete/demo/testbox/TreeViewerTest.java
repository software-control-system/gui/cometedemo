package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JPanel;

import fr.soleil.comete.box.complexbox.TreeViewerBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.simulated.data.service.SimulatedKey;
import fr.soleil.comete.simulated.data.service.SimulatedKeyGenerator;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.Tree;
import fr.soleil.data.service.IKey;

public class TreeViewerTest {

    private static List<IKey> randomKeys = new ArrayList<IKey>();
    private static TreeViewerBox treeBox = new TreeViewerBox();
    private static Tree tree = new Tree();

    private static JPanel initPanel() {
        final JPanel panel = new JPanel(new BorderLayout());

        SimulatedKeyGenerator generator = new SimulatedKeyGenerator();

        for (int i = 0; i < 7; ++i) {
            IKey key = null;
            if (i == 0) {
                key = generator.getBooleanMatrixKey();
            }
            if (i == 1) {
                key = generator.getBooleanScalarKey();
            }
            if (i == 2) {
                key = generator.getChartKey();
            }
            if (i == 3) {
                key = generator.getImageNumberMatrixKey();
            }
            if (i == 4) {
                key = generator.getNumberScalarKey();
            }
            if (i == 5) {
                key = generator.getStringScalarKey();
            }
            if (i == 6) {
                key = generator.getStringMatrixKey();
            }
            SimulatedKey.registerTreeKey(key);
            randomKeys.add(key);
        }

        treeBox.connectWidget(tree, randomKeys.get(0));

        panel.add(tree, BorderLayout.CENTER);

        final JButton button = new JButton("New Node");
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent arg0) {
                Random rand = new Random();
                int index = rand.nextInt(6);
                treeBox.connectWidget(tree, randomKeys.get(index));
                System.out.println("tests");
                tree.repaint();
            }
        });

        panel.add(button, BorderLayout.SOUTH);

        return panel;
    }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        final JPanel f = initPanel();
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("TextField Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
