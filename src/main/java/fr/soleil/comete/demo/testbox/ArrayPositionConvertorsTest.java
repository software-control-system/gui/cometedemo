package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.util.Arrays;

import javax.swing.JComponent;
import javax.swing.JPanel;

import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.IImageViewer;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.comete.tango.data.service.helper.TangoDeviceHelper;

public class ArrayPositionConvertorsTest {

    private static String deviceName = "ica/salsa/scan.2";
    private static String imageName = "data_01";
    private static String xAttributeName = "actuator_1_1";
    private static String yAttributeName = "actuator_2_1";

    private static JPanel initPanel(IImageViewer imageViewer) {

        final JPanel panel = new JPanel(new BorderLayout());
        if (imageViewer instanceof JComponent) {
            panel.add((JComponent) imageViewer, BorderLayout.CENTER);
        }

        return panel;
    }

    private static IImageViewer createImageViewer() {

        IImageViewer viewer = new ImageViewer();
        viewer.setAlwaysFitMaxSize(true);
        NumberMatrixBox imageBox = new NumberMatrixBox();

        TangoKey imagekey = new TangoKey();
        TangoKeyTool.registerAttribute(imagekey, deviceName, imageName);
        imageBox.connectWidget(viewer, imagekey);

        // IMAGE ON X
        ArrayPositionConvertor xConvertor = new ArrayPositionConvertor();
        TangoKey xkey = new TangoKey();
        TangoKeyTool.registerAttribute(xkey, deviceName, xAttributeName);
        imageBox.connectWidget(xConvertor, xkey);

        // SPECTRUM ON Y
        ArrayPositionConvertor yConvertor = new ArrayPositionConvertor();
        TangoKey ykey = new TangoKey();
        TangoKeyTool.registerAttribute(ykey, deviceName, yAttributeName);
        imageBox.connectWidget(yConvertor, ykey);

        viewer.setXAxisConvertor(xConvertor);
        viewer.setYAxisConvertor(yConvertor);

        return viewer;
    }

    private static void displayValues() {
        DeviceProxy proxy = TangoDeviceHelper.getDeviceProxy(deviceName);
        if (proxy != null) {
            try {
                DeviceAttribute xAttribute = proxy.read_attribute(xAttributeName);
                double[] xDoubleValues = xAttribute.extractDoubleArray();
                System.out.println("x values = " + Arrays.toString(xDoubleValues));

                DeviceAttribute yAttribute = proxy.read_attribute(yAttributeName);
                short[] yDoubleValues = yAttribute.extractShortArray();
                System.out.println("y values = " + Arrays.toString(yDoubleValues));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        IImageViewer image = createImageViewer();
        displayValues();

        final JPanel f = initPanel(image);
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("NumberField Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}
