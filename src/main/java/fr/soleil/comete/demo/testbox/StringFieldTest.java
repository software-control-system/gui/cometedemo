package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.FileBrowser;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class StringFieldTest {

    private static JPanel initPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        StringScalarBox stringBox = new StringScalarBox();

        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titanZ", "string_scalar");

        TangoKey otherKey = new TangoKey();
        TangoKeyTool.registerSettable(otherKey, false);
        TangoKeyTool.registerAttribute(otherKey, "tango/tangotest/titan", "string_scalar");

        TextField textfield = new TextField();
        TextArea textArea = new TextArea();
        FileBrowser fileBrowser = new FileBrowser();

        stringBox.setConfirmationMessage(textfield, "Execute Confirmation ");
        stringBox.setConfirmation(textfield, true);

        fileBrowser.setUpdateAllowed(true);

        stringBox.connectWidget(textfield, key);
        stringBox.connectWidget(textArea, key);
        stringBox.connectWidget(fileBrowser, key);

        stringBox.setErrorText(textfield, "ERROR");
        stringBox.setConfirmation(textfield, true);
        stringBox.putInError(textfield);

        panel.add(textfield, BorderLayout.CENTER);
        panel.add(textArea, BorderLayout.EAST);
        panel.add(fileBrowser, BorderLayout.NORTH);

        final JLabel label = new JLabel("Click here to loose focus");

        panel.add(label, BorderLayout.SOUTH);
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                label.grabFocus();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });

        // System.out.println(stringBox.printSelfDescription());

        return panel;
    }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        final JPanel f = initPanel();
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("TextField Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
