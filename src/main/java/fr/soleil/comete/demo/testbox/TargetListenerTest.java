package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.service.ITargetListener;
import fr.soleil.comete.swing.FileBrowser;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class TargetListenerTest implements ITargetListener<String> {

    private JPanel initPanel() {
        final JPanel panel = new JPanel(new BorderLayout());

        StringScalarBox stringBox = new StringScalarBox();

        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "string_scalar");

        TextField textfield = new TextField();
        TextArea textArea = new TextArea();
        FileBrowser fileBrowser = new FileBrowser();

        stringBox.addTargetListener(this, textfield);

        stringBox.connectWidget(textfield, key);
        stringBox.connectWidget(textArea, key);
        stringBox.connectWidget(fileBrowser, key);

        fileBrowser.setUpdateAllowed(true);

        panel.add(textfield, BorderLayout.CENTER);
        panel.add(textArea, BorderLayout.EAST);
        panel.add(fileBrowser, BorderLayout.NORTH);

        final JLabel label = new JLabel("Click here to loose focus");

        panel.add(label, BorderLayout.SOUTH);
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                label.grabFocus();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });

        return panel;
    }

    @Override
    public void onTargetInput(String data) {
        System.out.println("data changed : " + data);
    }

    @Override
    public void onTargetOutput(String data) {
        System.out.println("text field changed : " + data);
    }

    public static void main(String[] args) {
        IFrame frame = new Frame();
        TargetListenerTest sourceTest = new TargetListenerTest();

        final JPanel f = sourceTest.initPanel();
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("TextField Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
