package fr.soleil.comete.demo.testbox;

import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.soleil.comete.swing.ImageViewer;

public class CometeComponentsMemoryLeakTest {

    public Runnable createImageCreatorThread() {
        Runnable result = new Runnable() {
            int number = 0;

            @Override
            public void run() {
                while (true) {
                    // Replace with the class you want to test
                    new ImageViewer();
                    System.out.println("CREATED " + (++number) + " instance(s)");
                    try {
                        Thread.sleep(150);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        return result;
    }

    public AbstractAction createGarbageCollectAction() {
        @SuppressWarnings("serial")
        AbstractAction result = new AbstractAction("Garbage Collect!") {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.gc();
                System.out.println("-- Garbage Collected --");
            }
        };
        return result;
    }

    public static void main(String[] args) throws InvocationTargetException, InterruptedException {
        final CometeComponentsMemoryLeakTest test = new CometeComponentsMemoryLeakTest();
        SwingUtilities.invokeAndWait(new Runnable() {

            @Override
            public void run() {
                JFrame mainFrame = new JFrame("Test");
                mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                mainFrame.setSize(300, 300);
                JPanel mainPanel = new JPanel();
                mainFrame.add(mainPanel);
                JButton button = new JButton(test.createGarbageCollectAction());
                mainPanel.add(button);
                mainFrame.setVisible(true);

            }
        });
        test.createImageCreatorThread().run();
    }
}