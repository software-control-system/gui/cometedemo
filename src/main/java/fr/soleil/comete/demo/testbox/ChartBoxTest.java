package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class ChartBoxTest extends JPanel {

    private static final long serialVersionUID = 324215453310074770L;

    private final Chart chart = new Chart();
    private final ChartViewerBox chartBox = new ChartViewerBox();
    private final TangoKey key = new TangoKey();

    public ChartBoxTest() {
        super(new BorderLayout());
        chart.setAutoHighlightOnLegend(true);
        add(chart, BorderLayout.CENTER);
        JPanel buttonPanel = new JPanel();
        JButton connectButton = new JButton("Connect");
        JButton disconnectButton = new JButton("Disconnect");
        buttonPanel.add(connectButton);
        buttonPanel.add(disconnectButton);
        add(buttonPanel, BorderLayout.SOUTH);
        TangoKeyTool.registerAttribute(key, "tango/tangotest/1/wave");
        connectButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                connect();
            }
        });

        disconnectButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                disconnect();
            }
        });
    }

    private void connect() {
        chartBox.connectWidget(chart, key);
    }

    private void disconnect() {
        chartBox.disconnectWidget(chart, key);
    }

    public static void main(String[] args) {
        System.setProperty("TANGO_HOST", "calypso:20001");
        ChartBoxTest chart = new ChartBoxTest();
        JFrame testFrame = new JFrame(ChartBoxTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(chart);
        testFrame.setSize(600, 700);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }
}
