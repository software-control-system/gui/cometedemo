package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;

import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class ImageInChartTest {

    private static final ChartViewerBox CHART_BOX = new ChartViewerBox();
    private static final IChartViewer VIEWER = new Chart();

    private static JPanel initPanel(IChartViewer chartViewer) {

        final JPanel panel = new JPanel(new BorderLayout());

        if (chartViewer instanceof JComponent) {
            panel.add((JComponent) chartViewer, BorderLayout.CENTER);
        }

        final JButton disconnectButton = new JButton("Disconnect Widget");
        disconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                CHART_BOX.disconnectWidgetFromAll(VIEWER);
            }
        });

        panel.add(disconnectButton, BorderLayout.SOUTH);

        return panel;
    }

    private static IChartViewer createChartViewer() {

        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_image_ro");
        CHART_BOX.connectWidget(VIEWER, key);

        return VIEWER;
    }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        IChartViewer chart = createChartViewer();

        final JPanel f = initPanel(chart);
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("NumberField Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}
