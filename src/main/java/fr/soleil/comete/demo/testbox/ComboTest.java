package fr.soleil.comete.demo.testbox;

import javax.swing.JPanel;

import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.BooleanComboBox;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.NumberComboBox;
import fr.soleil.comete.swing.StringMatrixComboBoxViewer;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.service.DataSourceProducerProvider;

public class ComboTest {

    private static JPanel initPanel() {
        final JPanel panel = new JPanel();

        StringMatrixBox stringMatrixBox = new StringMatrixBox();
        StringScalarBox stringBox = new StringScalarBox();
        BooleanScalarBox booleanScalarBox = new BooleanScalarBox();
        NumberScalarBox numberScalarBox = new NumberScalarBox();

        TangoKey stringMatrixKey = new TangoKey();
        TangoKeyTool.registerAttribute(stringMatrixKey, "tango/tangotest/1", "string_spectrum_ro");
        TangoKey stringScalarKey = new TangoKey();
        TangoKeyTool.registerAttribute(stringScalarKey, "tango/tangotest/1", "string_scalar");
        TangoKey stringScalarWriteKey = new TangoKey();
        TangoKeyTool.registerWriteAttribute(stringScalarWriteKey, "tango/tangotest/1", "string_scalar");
        TangoKey booleanScalarWriteKey = new TangoKey();
        TangoKeyTool.registerWriteAttribute(booleanScalarWriteKey, "tango/tangotest/1", "boolean_scalar");

        TangoKey numberScalarKey = new TangoKey();
        TangoKeyTool.registerAttribute(numberScalarKey, "tango/tangotest/1", "ampli");
        TangoKey numberScalarWriteKey = new TangoKey();
        TangoKeyTool.registerWriteAttribute(numberScalarWriteKey, "tango/tangotest/1", "ampli");

        Label stringLabel = new Label();
        Label numberLabel = new Label();
        Label booleanLabel = new Label();

        ComboBox stringCombo = new ComboBox();
        NumberComboBox numberCombo = new NumberComboBox();
        BooleanComboBox booleanCombo = new BooleanComboBox();
        StringMatrixComboBoxViewer matrixCombo = new StringMatrixComboBoxViewer();
        matrixCombo.setLinkPopupVisibilityWithEditable(false);

        stringCombo.setObjectArray(new Object[] { "value1", "value2", "value3" });
        stringCombo.setDisplayedList(new String[] { "Label 1", "Label 2", "Label 3" });
        // stringCombo.setEditable(false);

        numberCombo.setObjectArray(new Object[] { Double.valueOf(1), Double.valueOf(2), Double.valueOf(3) });
        numberCombo.setDisplayedList(new String[] { "Label 1", "Label 2", "Label 3" });
        // numberCombo.setEditable(false);

        stringBox.connectWidget(stringLabel, stringScalarKey);
        stringBox.connectWidget(stringCombo, stringScalarWriteKey);
        stringBox.connectWidget(numberLabel, numberScalarKey);
        numberScalarBox.connectWidget(numberCombo, numberScalarWriteKey);
        booleanScalarBox.connectWidget(booleanCombo, booleanScalarWriteKey);
        stringBox.connectWidget(booleanLabel, booleanScalarWriteKey);
        stringMatrixBox.connectWidget(matrixCombo, stringMatrixKey);
        TangoDataSourceFactory factory = (TangoDataSourceFactory) DataSourceProducerProvider
                .getProducerByClassName(TangoDataSourceFactory.class.getName());
        factory.setRefreshingStrategy(stringMatrixKey, null);

        panel.add(stringLabel);
        panel.add(stringCombo);
        panel.add(numberLabel);
        panel.add(numberCombo);
        panel.add(booleanCombo);
        panel.add(booleanLabel);
        panel.add(matrixCombo);

        return panel;
    }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        final JPanel f = initPanel();
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("Label Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
