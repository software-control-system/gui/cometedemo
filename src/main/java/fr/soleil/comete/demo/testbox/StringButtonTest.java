package fr.soleil.comete.demo.testbox;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class StringButtonTest {

    public static void main(String[] args) {
        StringButton button = new StringButton();
        String commandName = "init";
        String deviceName = "tango/tangotest/titan";
        TangoKey key = new TangoKey();
        TangoKeyTool.registerCommand(key, deviceName, commandName);
        button.setText(commandName);
        StringScalarBox box = new StringScalarBox();
        JFrame frame = new JFrame(StringButtonTest.class.getSimpleName() + " " + deviceName + "/" + commandName);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(button);
        frame.pack();
        frame.setSize(500, frame.getHeight());
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        box.connectWidget(button, key);
    }

}
