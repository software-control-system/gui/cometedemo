package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.AbstractTable;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.NumberMatrixTable;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class MatrixFieldTest {

    private static JPanel initMatrixPanel(AbstractTable<?> matrixTable) {

        final JPanel panel = new JPanel(new BorderLayout());

        panel.add(matrixTable, BorderLayout.CENTER);

        final JLabel label = new JLabel("Click here to loose focus");

        panel.add(label, BorderLayout.SOUTH);
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                label.grabFocus();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });

        return panel;
    }

    private static AbstractTable<?> createNumberMatrix() {

        NumberMatrixTable table = new NumberMatrixTable();

        NumberMatrixBox numberMBox = new NumberMatrixBox();

        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_image_ro");

        numberMBox.connectWidget(table, key);

        return table;
    }

    // private static AbstractTable<?> createStringMatrix() {
    //
    // StringMatrixTable table = new StringMatrixTable();
    //
    // StringMatrixBox stringMBox = new StringMatrixBox();
    //
    // TangoKey key = new TangoKey();
    // TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "string_image_ro");
    //
    // stringMBox.connectWidget(table, key);
    //
    // return table;
    // }
    //
    // private static AbstractTable<?> createBooleanMatrix() {
    //
    // BooleanMatrixTable table = new BooleanMatrixTable();
    //
    // BooleanMatrixBox booleanMBox = new BooleanMatrixBox();
    //
    // TangoKey key = new TangoKey();
    // TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "boolean_image_ro");
    //
    // booleanMBox.connectWidget(table, key);
    //
    // return table;
    // }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        AbstractTable<?> table = createNumberMatrix();

        final JPanel f = initMatrixPanel(table);
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("NumberField Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
