package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.service.ITargetListener;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class CommandTest {

    private static JPanel initPanel() {
        IFrame frame = new Frame();

        // ajout de frame gênantes pour tester que les popups se rattachent bien au bon parent
        frame.setSize(300, 300);
        frame.setPreferredSize(300, 300);
        frame.setEnabled(true);
        frame.setVisible(true);

        final JPanel panel = new JPanel(new BorderLayout());
        StringScalarBox stringBox = new StringScalarBox();

        TangoKey key = new TangoKey();
        TangoKeyTool.registerCommand(key, "tango/tangotest/titan", "DevVarDoubleArray");

        StringButton commandButton = new StringButton();
        commandButton.setButtonLook(true);

        stringBox.setConfirmation(commandButton, false);
        stringBox.setOutputInPopup(commandButton, true);
        stringBox.setCommandWidget(commandButton, true);
        stringBox.connectWidget(commandButton, key);
        // commandButton.setParameter("1,2,3");

        stringBox.addTargetListener(new ITargetListener<Object>() {
            @Override
            public void onTargetInput(Object data) {
                System.out.println("Woohoo Input detected");

            }

            @Override
            public void onTargetOutput(Object data) {
                System.out.println("Woohoo Output detected");

            }

        }, commandButton);

        panel.add(commandButton, BorderLayout.CENTER);

        final JLabel label = new JLabel("Click here to loose focus");

        panel.add(label, BorderLayout.SOUTH);
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                label.grabFocus();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });

        return panel;
    }

    public static void main(String[] args) {
        IFrame ndFrame = new Frame();

        // ajout de frame gênantes pour tester que les popups se rattachent bien au bon parent
        ndFrame.setSize(300, 300);
        ndFrame.setPreferredSize(300, 300);
        ndFrame.setEnabled(true);
        ndFrame.setVisible(true);

        IFrame frame = new Frame();

        final JPanel f = initPanel();
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("Command Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        f.grabFocus();

    }
}
