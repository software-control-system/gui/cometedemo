package fr.soleil.comete.demo.testbox;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import fr.soleil.comete.swing.TextField;

public class TextFieldTest {

    public static void main(String[] args) {
        JFrame frame = new JFrame(TextFieldTest.class.getSimpleName());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        TextField field = new TextField();
        field.setColumns(20);
        frame.setContentPane(field);
        frame.setSize(200, 100);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

}
