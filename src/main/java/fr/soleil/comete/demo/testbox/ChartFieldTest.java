package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class ChartFieldTest {

    private static JComponent initPanel(IChartViewer chartViewer) {
        final JPanel panel = new JPanel(new BorderLayout());

        if (chartViewer instanceof JComponent) {
            panel.add((JComponent) chartViewer, BorderLayout.CENTER);
        }

        final JLabel label = new JLabel("Click here to loose focus");

        panel.add(label, BorderLayout.SOUTH);
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                label.grabFocus();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("To Hide", new JLabel("Tab that should hide chart"));
        tabbedPane.addTab("Chart", panel);

        return tabbedPane;
    }

    private static Chart createChart() {
        Chart viewer = new Chart();

        ChartViewerBox chartBox = new ChartViewerBox();

        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_spectrum_ro");

        chartBox.connectWidget(viewer, key);

        return viewer;
    }

    // private static IChartViewer createChartPlayer() {
    //
    // IChartViewer viewer = new ChartPlayer();
    //
    // ChartViewerBox chartBox = new ChartViewerBox();
    //
    // TangoKey key = new TangoKey();
    // TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_spectrum_ro");
    //
    // chartBox.connectWidget(viewer, key);
    //
    // return viewer;
    // }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        IChartViewer chart = createChart();

        final JComponent f = initPanel(chart);
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("NumberField Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
