package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.swing.FileBrowser;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.Slider;
import fr.soleil.comete.swing.Spinner;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class MultiFieldTest {

    private static JPanel initPanel() {

        final JPanel panel = new JPanel(new BorderLayout());

        JPanel stringPanel = new JPanel(new BorderLayout());
        JPanel numberPanel = new JPanel(new BorderLayout());

        StringScalarBox stringBox = new StringScalarBox();
        NumberScalarBox numberBox = new NumberScalarBox();

        TangoKey stringKey = new TangoKey();
        TangoKeyTool.registerAttribute(stringKey, "tango/tangotest/titan", "string_scalar");

        TangoKey stringImageKey = new TangoKey();
        TangoKeyTool.registerAttribute(stringImageKey, "tango/tangotest/titan", "string_spectrum_ro");

        TangoKey numberKey = new TangoKey();
        TangoKeyTool.registerAttribute(numberKey, "tango/tangotest/titan", "ampli");

        // text component
        TextField textfield = new TextField();
        TextArea textArea = new TextArea();
        FileBrowser fileBrowser = new FileBrowser();

        stringBox.connectWidget(textfield, stringKey);
        stringBox.connectWidget(textArea, stringImageKey);
        stringBox.connectWidget(fileBrowser, stringKey);

        fileBrowser.setUpdateAllowed(true);

        // added to stringPanel component
        stringPanel.add(textfield, BorderLayout.WEST);
        stringPanel.add(textArea, BorderLayout.CENTER);
        stringPanel.add(fileBrowser, BorderLayout.NORTH);

        // number component
        Spinner spinner = new Spinner();
        Slider slider = new Slider();
        WheelSwitch wheelswitch = new WheelSwitch();

        // numberBox.connectWidget(spinner, stringKey);
        numberBox.connectWidget(spinner, numberKey);
        numberBox.connectWidget(slider, numberKey);
        numberBox.connectWidget(wheelswitch, numberKey);

        // added to numberPanel component
        numberPanel.add(spinner, BorderLayout.CENTER);
        numberPanel.add(wheelswitch, BorderLayout.EAST);
        numberPanel.add(slider, BorderLayout.NORTH);

        panel.add(stringPanel, BorderLayout.CENTER);
        panel.add(numberPanel, BorderLayout.EAST);

        final JLabel label = new JLabel("Click here to loose focus");

        panel.add(label, BorderLayout.SOUTH);
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                label.grabFocus();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });

        return panel;
    }

    public static void main(String[] args) {
        IFrame frame = new Frame();

        final JPanel f = initPanel();
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle(MultiFieldTest.class.getSimpleName());
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
