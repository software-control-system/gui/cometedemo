package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.demo.simulated.data.service.TangoKeyGenerator;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.action.ChangeRefreshingPeriodAction;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;

public class RefreshingStrategyTest {

    public static void main(String[] args) {
        TangoKeyGenerator generator = new TangoKeyGenerator();
        StringScalarBox box = new StringScalarBox();
        Label label = new Label();
        IKey key = generator.getNumberScalarKey();
        box.connectWidget(label, key);
        JFrame frame = new JFrame(RefreshingStrategyTest.class.getSimpleName() + ": " + key.getInformationKey());
        for (String arg : args) {
            try {
                int period = Integer.parseInt(arg.trim());
                if (period > 0) {
                    DataSourceProducerProvider.setRefreshingStrategy(key, new PolledRefreshingStrategy(period));
                }
            } catch (Exception e) {
                // ignore
            }
        }
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(label, BorderLayout.NORTH);
        ChangeRefreshingPeriodAction action = new ChangeRefreshingPeriodAction();
        action.setKey(key);
        JButton button = new JButton(action);
        mainPanel.add(button, BorderLayout.SOUTH);
        frame.setContentPane(mainPanel);
        frame.setSize(500, 150);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

}
