package fr.soleil.comete.demo.testbox;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.service.ITargetListener;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.FileBrowser;
import fr.soleil.comete.swing.Frame;
import fr.soleil.comete.swing.Spinner;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class ConnectionTest implements ITargetListener<Boolean> {

    private static final String DEVICE = "tango/tangotest/titan";

    private final StringScalarBox stringBox = new StringScalarBox();
    private final NumberScalarBox numberBox = new NumberScalarBox();
    private final BooleanScalarBox booleanBox = new BooleanScalarBox();

    private final TextField textfield = new TextField();
    private final Spinner spinner = new Spinner();
    private final FileBrowser fileBrowser = new FileBrowser();
    private final CheckBox switchButton = new CheckBox();

    private final TangoKey stringKey = new TangoKey();
    private final TangoKey numberKey = new TangoKey();

    private JPanel initPanel() {
        switchButton.setTrueLabel("Connected");
        switchButton.setFalseLabel("Disconnected");
        final JPanel panel = new JPanel(new BorderLayout());

        TangoKeyTool.registerAttribute(stringKey, DEVICE, "string_scalar");
        TangoKeyTool.registerAttribute(numberKey, DEVICE, "ampli");

        stringBox.connectWidget(textfield, stringKey);
        numberBox.connectWidget(spinner, numberKey);
        numberBox.setConfirmation(spinner, true);
        stringBox.connectWidget(fileBrowser, stringKey);
        fileBrowser.setUpdateAllowed(true);

        switchButton.setSelected(true);
        booleanBox.addTargetListener(this, switchButton);

        panel.add(textfield, BorderLayout.CENTER);
        panel.add(spinner, BorderLayout.EAST);
        panel.add(fileBrowser, BorderLayout.NORTH);
        panel.add(switchButton, BorderLayout.WEST);

        final JLabel label = new JLabel("Click here to loose focus");

        // final JPanel buttonPanel = new JPanel(new VerticalLayout());

        panel.add(label, BorderLayout.SOUTH);
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                label.grabFocus();
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }
        });

        return panel;
    }

    @Override
    public void onTargetInput(Boolean data) {
    }

    @Override
    public void onTargetOutput(Boolean data) {
        if (data) {
            stringBox.connectWidget(textfield, stringKey);
            numberBox.connectWidget(spinner, numberKey);
            stringBox.connectWidget(fileBrowser, stringKey);
        } else {
            stringBox.disconnectWidgetFromAll(textfield);
            numberBox.disconnectWidgetFromAll(spinner);
            stringBox.disconnectWidgetFromAll(fileBrowser);
        }

        System.out.println(stringBox.printSelfDescription());
        System.out.println(numberBox.printSelfDescription());
    }

    public static void main(String[] args) {
        IFrame frame = new Frame();
        ConnectionTest sourceTest = new ConnectionTest();

        final JPanel f = sourceTest.initPanel();
        f.setSize(300, 300);
        f.setPreferredSize(f.getSize());
        f.setEnabled(true);

        frame.setTitle("TextField Test");
        frame.setContentPane(f);
        frame.setSize(500, 200);
        frame.setTitle(f.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

}
