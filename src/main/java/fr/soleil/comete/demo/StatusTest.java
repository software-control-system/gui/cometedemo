package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.AutoScrolledTextField;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;

public class StatusTest extends JPanel {

    private static final long serialVersionUID = 6774162539320438039L;

    private final StringScalarBox stringScalarBox;
    private final AutoScrolledTextField textField;
    private final JButton connectionButton;

    public StatusTest() {
        super(new BorderLayout(5, 5));
        stringScalarBox = new StringScalarBox();
        textField = new AutoScrolledTextField();
        textField.setEditable(false);
        connectionButton = new JButton("Connect...");
        connectionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String toConnect = JOptionPane.showInputDialog(connectionButton, "Enter the device name...");
                if (toConnect != null) {
                    connect(toConnect);
                }
            }
        });
        add(textField, BorderLayout.CENTER);
        add(connectionButton, BorderLayout.SOUTH);
    }

    public void connect(String device) {
        stringScalarBox.disconnectWidgetFromAll(textField);
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, device, "status");
        stringScalarBox.connectWidget(textField, key);
    }

    public static void main(String[] args) {
        JFrame testFrame = new JFrame(StatusTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(new StatusTest());
        testFrame.setSize(600, 100);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

}
