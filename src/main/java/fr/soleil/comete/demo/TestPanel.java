// +============================================================================
// Source: package fr.soleil.comete.test;/TestPanel.java
//
// project : Comete
//
// Description: This class hides
//
// Author: SAINTIN
//
// Revision: 1.1
//
// Log:
//
// copyleft :Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
// FRANCE
//
// +============================================================================
package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.AbstractCometeBox;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.box.matrixbox.BooleanMatrixBox;
import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.box.scalarbox.NumberScalarBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.definition.listener.IComboBoxListener;
import fr.soleil.comete.definition.util.ArrayPositionConvertor;
import fr.soleil.comete.definition.util.IValueConvertor;
import fr.soleil.comete.definition.widget.IComponent;
import fr.soleil.comete.definition.widget.IFileBrowser;
import fr.soleil.comete.definition.widget.IPanel;
import fr.soleil.comete.definition.widget.IStringPeriodicTable;
import fr.soleil.comete.definition.widget.ITabbedPane;
import fr.soleil.comete.definition.widget.util.AngularSector;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.demo.simulated.data.service.TangoKeyGenerator;
import fr.soleil.comete.simulated.data.service.IKeyGenerator;
import fr.soleil.comete.simulated.data.service.SimulatedDataSourceProducer;
import fr.soleil.comete.simulated.data.service.SimulatedKeyGenerator;
import fr.soleil.comete.swing.BooleanComboBox;
import fr.soleil.comete.swing.BooleanMatrixTable;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.ChartPlayer;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.FileBrowser;
import fr.soleil.comete.swing.FileBrowserFieldButton;
import fr.soleil.comete.swing.IconButton;
import fr.soleil.comete.swing.ImagePlayer;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.NumberComboBox;
import fr.soleil.comete.swing.NumberMatrixTable;
import fr.soleil.comete.swing.NumberPeriodicTable;
import fr.soleil.comete.swing.Panel;
import fr.soleil.comete.swing.ProgressBar;
import fr.soleil.comete.swing.Slider;
import fr.soleil.comete.swing.Spinner;
import fr.soleil.comete.swing.StringButton;
import fr.soleil.comete.swing.StringMatrixTable;
import fr.soleil.comete.swing.StringPeriodicTable;
import fr.soleil.comete.swing.TabbedPane;
import fr.soleil.comete.swing.TextArea;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.TextFieldButton;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.swing.action.ChangeRefreshingPeriodAction;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.data.filter.MatrixSymmetry;
import fr.soleil.data.service.IKey;
import fr.soleil.data.target.ITarget;
import fr.soleil.lib.project.swing.SnapshotUtil;

public class TestPanel extends TabbedPane implements IComboBoxListener {

    private static final long serialVersionUID = -2838667446634779596L;

    private final ITabbedPane panel = new TabbedPane();

    private final Map<String, IKeyGenerator> keyGeneratorMap = new HashMap<String, IKeyGenerator>();

    // SIMPLE COMPONENT
    private final IPanel numberPanel;
    private final IPanel booleanPanel;
    private final IPanel textPanel;

    private final BooleanComboBox booleanComboBox = new BooleanComboBox();
    private final IconButton booleanIconButton = new IconButton();
    private final CheckBox checkBox = new CheckBox();

    private final TextField numberField = new TextField();
    private final WheelSwitch wheelswitch = new WheelSwitch();
    private final Spinner numberspinner = new Spinner();
    private final Slider numberslider = new Slider();
    private final ProgressBar progressBar = new ProgressBar();
    private final TextFieldButton numberFieldButton = new TextFieldButton();
    private final NumberPeriodicTable numberPeriodicTable = new NumberPeriodicTable();
    private final NumberComboBox numberCombo = new NumberComboBox();
    private final Label numberLabel = new Label();

    private final TextField textField = new TextField();
    private final TextFieldButton textFieldButton = new TextFieldButton();
    private final FileBrowser fileBrowserButton = new FileBrowser();
    private final FileBrowserFieldButton fileBrowserFieldButton = new FileBrowserFieldButton();
    private final TextArea textArea = new TextArea();
    private final ComboBox textCombo = new ComboBox();
    private final StringPeriodicTable periodicTable = new StringPeriodicTable();
    private final Label label = new Label();
    private final Label state1 = new Label();
    private final Label state2 = new Label();
    private final StringButton statusButton = new StringButton();

    // STRING ARRAY COMPONENT
    private final StringMatrixTable stringTable = new StringMatrixTable();

    // BOOLEAN ARRAY COMPONENT
    private final BooleanMatrixTable booleanTable = new BooleanMatrixTable();

    // IMAGE COMPONENT
    private final ChangeRefreshingPeriodAction changeRefreshingPeriodAction = new ChangeRefreshingPeriodAction();
    private final ImageViewer image = new ImageViewer();
    private final ImageViewer image2 = new ImageViewer();
    private final ImagePlayer imagePlayer = new ImagePlayer();
    private final JCheckBoxMenuItem randomScaleItem = new JCheckBoxMenuItem("Use random Y scale");
    private final NumberMatrixTable imageTable = new NumberMatrixTable();
    private final ArrayPositionConvertor xAxisConvertor = new ArrayPositionConvertor();
    private final ArrayPositionConvertor yAxisConvertor = new ArrayPositionConvertor();

    // CHART COMPONENT
    // private final ChartViewer chartViewer = new ChartViewer();
    private final Chart chart = new Chart();
    private final ChartPlayer chartPlayer = new ChartPlayer();

    private final StringScalarBox stringBox = new StringScalarBox();
    private final NumberScalarBox numberBox = new NumberScalarBox();
    private final BooleanScalarBox booleanBox = new BooleanScalarBox();

    private final StringMatrixBox stringMatrixBox = new StringMatrixBox();
    private final NumberMatrixBox numberMatrixBox = new NumberMatrixBox();
    private final BooleanMatrixBox booleanMatrixBox = new BooleanMatrixBox();

    private final ChartViewerBox chartBox = new ChartViewerBox();

    private final ProgressBar connectionProgress;
    private int ratio;
    private static Runnable threadProgressBar;

    private IKeyGenerator currentKeyGenerator;

    private final boolean debug;

    public TestPanel(boolean debug) {
        super();
        this.debug = debug;

        IKeyGenerator tangoKey = new TangoKeyGenerator();
        keyGeneratorMap.put(tangoKey.toString(), tangoKey);

        IKeyGenerator simulatedKey = new SimulatedKeyGenerator();
        keyGeneratorMap.put(simulatedKey.toString(), simulatedKey);

        // chartViewer.setFreezePanelVisible(true);
        // chartViewer.setManagementPanelVisible(true);
        // chartViewer.setAutoHighlightOnLegend(true);
        // chartViewer.setAxisSelectionVisible(true);
        // chartViewer.setAutoHideViews(true);
        chart.setCometeBackground(CometeColor.YELLOW);
        chart.setFreezePanelVisible(true);
        chart.setManagementPanelVisible(true);
        chart.setAutoHighlightOnLegend(true);
        chart.setAxisSelectionVisible(true);
        chart.setAutoHideViews(true);
        chart.setOpaque(false);

        periodicTable.setMode(IStringPeriodicTable.SYMBOL_MODE);
        periodicTable.setEnabled(true);

        booleanPanel = initBooleanPanel();
        textPanel = initStringPanel();
        numberPanel = initNumberPanel();

        panel.add("TEXT", textPanel);
        panel.add("NUMBER", numberPanel);
        panel.add("BOOLEAN", booleanPanel);

        imagePlayer.setPeriodInMs(100);
        imagePlayer.setAnimationButtonVisible(true);
        chartPlayer.setPeriodInMs(100);
        chartPlayer.setAnimationButtonVisible(true);

        image.setShowRoiInformationTable(true);
        image.setUseMaskManagement(true);
        image.registerSectorClass(AngularSector.class);

        image2.setShowRoiInformationTable(true);
        image2.setUseMaskManagement(true);
        image2.registerSectorClass(AngularSector.class);
        randomScaleItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IValueConvertor convertor;
                if (randomScaleItem.isSelected()) {
                    convertor = generateRandomScaleConvertor();
                } else {
                    convertor = null;
                }
                image2.setYAxisConvertor(convertor);
            }
        });
        image2.getPopupMenu().add(randomScaleItem);

        JPanel imageViewerPanel = new JPanel(new GridLayout(1, 2));
        imageViewerPanel.add(image);
        imageViewerPanel.add(image2);
        JPanel imagePanel = new JPanel(new BorderLayout());
        imagePanel.add(imageViewerPanel, BorderLayout.CENTER);
        imagePanel.add(new JButton(changeRefreshingPeriodAction), BorderLayout.NORTH);

        // JPanel chartViewerPanel = new JPanel(new BorderLayout());
        // chartViewerPanel.setBackground(Color.WHITE);
        // JLabel chartViewerTitle = new
        // JLabel(ChartViewer.class.getSimpleName(), JLabel.CENTER);
        // chartViewerPanel.add(chartViewerTitle, BorderLayout.NORTH);
        // chartViewerPanel.add(chartViewer, BorderLayout.CENTER);
        JPanel chartPanel = new JPanel(new BorderLayout());
        chartPanel.setBackground(new Color(240, 255, 240));
        JLabel chartTitle = new JLabel("Chart V2", JLabel.CENTER);
        chartPanel.add(chartTitle, BorderLayout.NORTH);
        chartPanel.add(chart, BorderLayout.CENTER);
        // JSplitPane chartsSplitPane = new
        // JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        // chartsSplitPane.setLeftComponent(chartViewerPanel);
        // chartsSplitPane.setRightComponent(chartPanel);
        // chartsSplitPane.setDividerSize(8);
        // chartsSplitPane.setDividerLocation(0.5);
        // chartsSplitPane.setResizeWeight(0.5);
        // chartsSplitPane.setOneTouchExpandable(true);

        JPanel panelConnection = new JPanel(new FlowLayout());
        JButton coButton = new JButton("Connect");
        coButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                initConnections();
            }
        });
        JButton discoButton = new JButton("Disconnect");
        discoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                clearBasicConnections();
            }
        });
        connectionProgress = new ProgressBar();
        connectionProgress.setValue(0);
        connectionProgress.setMaximum(100);
        connectionProgress.setMinimum(0);
        ratio = 3;
        threadProgressBar = new Runnable() {

            @Override
            public void run() {
                connectionProgress.setValue(connectionProgress.getValue() + ratio);
            }
        };

        panelConnection.add(coButton);
        panelConnection.add(discoButton);
        panelConnection.add(connectionProgress);

        add("Simple", panel);
        add("StringTable", (IComponent) stringTable);
        add("BooleanTable", (IComponent) booleanTable);
        add("Image", imagePanel);
        add("Image Table", (IComponent) imageTable);
        add("Image Player", (IComponent) imagePlayer);
        addTab("Charts", chartPanel);
        add("Chart Player", (IComponent) chartPlayer);
        add("Periodic Table", (IComponent) periodicTable);
        add("Number Periodic Table", (IComponent) numberPeriodicTable);
        add("Disconnect", panelConnection);

        image.setXAxisConvertor(xAxisConvertor);
        image.setYAxisConvertor(yAxisConvertor);

        grabFocus();
    }

    protected IValueConvertor generateRandomScaleConvertor() {
        double[] converted = new double[250];
        for (int i = 0; i < converted.length; i++) {
            double value = Math.random() * Math.pow(10, new Random().nextInt(5)) * (Math.random() > 0.5 ? 1 : -1);
            converted[i] = value;
        }
        ArrayPositionConvertor convertor = new ArrayPositionConvertor();
        convertor.setNumberArray(converted);
        return convertor;
    }

    private IPanel initNumberPanel() {
        IPanel result = new Panel();
        result.setTitledBorder("NUMERIC");
        result.add(numberField);
        result.add(wheelswitch);
        result.add(numberspinner);
        result.add(numberFieldButton);
        numberslider.setMaximum(100);
        numberslider.setMinimum(0);
        numberslider.setMinorTickSpacing(5);
        numberslider.setMajorTickSpacing(10);
        result.add(numberslider);
        progressBar.setMaximum(100);
        progressBar.setMinimum(0);
        result.add(progressBar);
        numberCombo.setNumberArray(new Number[] { 0, 1, 2 });
        numberCombo.setDisplayedList(new String[] { "Val0", "Val1", "Val2" });
        // TODO numberCombo.setConfirmation(true);
        result.add(numberCombo);
        numberLabel.setOpaque(true);
        result.add(numberLabel);

        return result;
    }

    private IPanel initStringPanel() {
        IPanel result = new Panel();
        statusButton.setText("STATUS");
        statusButton.setButtonLook(true);
        result.add(textField);
        result.add(textFieldButton);
        fileBrowserButton.setBrowserType(IFileBrowser.PATH);
        fileBrowserButton.setSize(400, 25);
        fileBrowserFieldButton.setBrowserType(IFileBrowser.PATH);
        textCombo.setValueList((Object[]) new String[] { "Hello", "Bonjour", "Coucou" });
        // TODO textCombo.setConfirmation(true);
        result.add(fileBrowserButton);
        result.add(fileBrowserFieldButton);
        result.add(textArea);
        result.add(textCombo);
        result.add(label);
        result.add(state1);
        result.add(state2);
        result.add(statusButton);
        return result;
    }

    private IPanel initBooleanPanel() {
        IPanel result = new Panel();
        result.setTitledBorder("BOOLEAN");
        result.add(booleanComboBox);
        result.add(checkBox);
        result.add(booleanIconButton);
        return result;
    }

    private synchronized void initConnections() {

        IKey currentKey = currentKeyGenerator.getBooleanScalarKey();

        booleanBox.reconnectWidget(checkBox, currentKey);
        booleanBox.reconnectWidget(booleanIconButton, currentKey);
        stringBox.reconnectWidget(booleanComboBox, currentKey);

        currentKey = currentKeyGenerator.getNumberScalarKey();
        stringBox.reconnectWidget(textField, currentKey);
        currentKey = currentKeyGenerator.getStringScalarKey();
        stringBox.reconnectWidget(textFieldButton, currentKey);
        stringBox.reconnectWidget(fileBrowserButton, currentKey);
        stringBox.reconnectWidget(fileBrowserFieldButton, currentKey);
        stringBox.reconnectWidget(textArea, currentKey);
        stringBox.reconnectWidget(textCombo, currentKey);
        stringBox.reconnectWidget(label, currentKey);
        stringBox.reconnectWidget(periodicTable, currentKey);
        currentKey = currentKeyGenerator.getStringScalarKey2();
        stringBox.reconnectWidget(state1, currentKey);
        stringBox.reconnectWidget(state2, currentKey);

        stringBox.reconnectWidget(statusButton, currentKey);

        currentKey = currentKeyGenerator.getNumberScalarKey();

        stringBox.reconnectWidget(numberField, currentKey);
        stringBox.reconnectWidget(numberFieldButton, currentKey);
        stringBox.reconnectWidget(numberLabel, currentKey);

        numberBox.reconnectWidget(numberCombo, currentKey);
        numberBox.reconnectWidget(wheelswitch, currentKey);
        numberBox.reconnectWidget(numberspinner, currentKey);
        numberBox.reconnectWidget(numberslider, currentKey);
        numberBox.reconnectWidget(progressBar, currentKey);
        numberBox.reconnectWidget(numberPeriodicTable, currentKey);

        currentKey = currentKeyGenerator.getBooleanMatrixKey();
        booleanMatrixBox.reconnectWidget(booleanTable, currentKey);

        currentKey = currentKeyGenerator.getStringMatrixKey();

        stringMatrixBox.reconnectWidget(stringTable, currentKey);

        currentKey = currentKeyGenerator.getChartKey();

        // chartBox.reconnectWidget(chartViewer, currentKey);
        chartBox.reconnectWidget(chart, currentKey);
        chartBox.reconnectWidget(chartPlayer, currentKey);

        currentKey = currentKeyGenerator.getSpectrumNumberMatrixKey();
        numberMatrixBox.reconnectWidget(xAxisConvertor, currentKey);
        numberMatrixBox.reconnectWidget(yAxisConvertor, currentKey);

        currentKey = currentKeyGenerator.getImageNumberMatrixKey();
        numberMatrixBox.reconnectWidget(imageTable, currentKey);

        numberMatrixBox.reconnectWidget(image, currentKey);
        numberMatrixBox.reconnectWidget(image2, currentKey);
        MatrixSymmetry symmetry = new MatrixSymmetry(new MatrixSymmetry(MatrixSymmetry.HORIZONTAL),
                MatrixSymmetry.VERTICAL);
        numberMatrixBox.addFilterToTarget(symmetry, image2);
        numberMatrixBox.reconnectWidget(imagePlayer, currentKey);
        changeRefreshingPeriodAction.setKey(currentKey);

        image.setImageName("Image, dynamic scale");
        image2.setImageName("Image, rotated, classic scale");
        if (randomScaleItem.isSelected()) {
            image2.setYAxisConvertor(generateRandomScaleConvertor());
        }

        if (debug) {
            System.out.println(stringBox.printSelfDescription());
            System.out.println(booleanBox.printSelfDescription());
            System.out.println(numberBox.printSelfDescription());
            System.out.println(stringMatrixBox.printSelfDescription());
            System.out.println(booleanMatrixBox.printSelfDescription());
            System.out.println(numberMatrixBox.printSelfDescription());
            System.out.println(chartBox.printSelfDescription());
        }
    }

    public synchronized void switchConnections(String producerId) {
        clearConnections();
        IKeyGenerator keyGen = keyGeneratorMap.get(producerId);
        if (keyGen != null) {
            currentKeyGenerator = keyGen;
            initConnections();
        }
    }

    private synchronized void clearConnections() {
        booleanBox.disconnectWidgetFromAll(checkBox);
        booleanBox.disconnectWidgetFromAll(booleanIconButton);

        stringBox.disconnectWidgetFromAll(booleanComboBox);
        stringBox.disconnectWidgetFromAll(textField);
        stringBox.disconnectWidgetFromAll(textFieldButton);
        stringBox.disconnectWidgetFromAll(fileBrowserButton);
        stringBox.disconnectWidgetFromAll(fileBrowserFieldButton);
        stringBox.disconnectWidgetFromAll(textArea);
        stringBox.disconnectWidgetFromAll(textCombo);
        stringBox.disconnectWidgetFromAll(label);
        stringBox.disconnectWidgetFromAll(periodicTable);
        stringBox.disconnectWidgetFromAll(numberField);
        stringBox.disconnectWidgetFromAll(numberFieldButton);
        stringBox.disconnectWidgetFromAll(numberLabel);

        numberBox.disconnectWidgetFromAll(numberCombo);
        numberBox.disconnectWidgetFromAll(wheelswitch);
        numberBox.disconnectWidgetFromAll(numberspinner);
        numberBox.disconnectWidgetFromAll(numberslider);
        numberBox.disconnectWidgetFromAll(progressBar);
        numberBox.disconnectWidgetFromAll(numberPeriodicTable);

        booleanMatrixBox.disconnectWidgetFromAll(booleanTable);

        stringMatrixBox.disconnectWidgetFromAll(stringTable);

        // chartBox.disconnectWidgetFromAll(chartViewer);
        chartBox.disconnectWidgetFromAll(chart);
        chartBox.disconnectWidgetFromAll(chartPlayer);

        numberMatrixBox.disconnectWidgetFromAll(imageTable);
        numberMatrixBox.disconnectWidgetFromAll(xAxisConvertor);
        numberMatrixBox.disconnectWidgetFromAll(yAxisConvertor);

        numberMatrixBox.disconnectWidgetFromAll(image);
        numberMatrixBox.disconnectWidgetFromAll(image2);
        numberMatrixBox.addFilterToTarget(null, image2);
        numberMatrixBox.disconnectWidgetFromAll(imagePlayer);
        changeRefreshingPeriodAction.setKey(null);
    }

    public void clearBasicConnections() {
        ratio = 4;
        progressBar.setValue(0);

        IKey currentKey = currentKeyGenerator.getBooleanScalarKey();
        graphicalDisconnection(booleanBox, checkBox, currentKey);
        graphicalDisconnection(booleanBox, booleanIconButton, currentKey);
        graphicalDisconnection(stringBox, booleanComboBox, currentKey);
        //
        // booleanBox.disconnectWidget(checkBox, currentKey);
        // booleanBox.disconnectWidget(booleanIconButton, currentKey);
        // stringBox.disconnectWidget(booleanComboBox, currentKey);

        currentKey = currentKeyGenerator.getNumberScalarKey();
        graphicalDisconnection(stringBox, textField, currentKey);

        currentKey = currentKeyGenerator.getStringScalarKey();
        graphicalDisconnection(stringBox, textFieldButton, currentKey);
        graphicalDisconnection(stringBox, fileBrowserButton, currentKey);
        graphicalDisconnection(stringBox, fileBrowserFieldButton, currentKey);
        graphicalDisconnection(stringBox, textArea, currentKey);
        graphicalDisconnection(stringBox, textCombo, currentKey);
        graphicalDisconnection(stringBox, label, currentKey);
        graphicalDisconnection(stringBox, periodicTable, currentKey);
        // currentKey = currentKeyGenerator.getStringScalarKey2();
        // stringBox.disconnectWidget(state1, currentKey);
        // stringBox.disconnectWidget(state2, currentKey);
        // stringBox.disconnectWidget(statusButton, currentKey);

        currentKey = currentKeyGenerator.getNumberScalarKey();

        graphicalDisconnection(stringBox, numberField, currentKey);
        graphicalDisconnection(stringBox, numberFieldButton, currentKey);
        graphicalDisconnection(stringBox, numberLabel, currentKey);

        graphicalDisconnection(numberBox, numberCombo, currentKey);
        graphicalDisconnection(numberBox, wheelswitch, currentKey);
        graphicalDisconnection(numberBox, numberspinner, currentKey);
        graphicalDisconnection(numberBox, numberslider, currentKey);
        graphicalDisconnection(numberBox, progressBar, currentKey);
        graphicalDisconnection(numberBox, numberPeriodicTable, currentKey);

        currentKey = currentKeyGenerator.getBooleanMatrixKey();
        graphicalDisconnection(booleanMatrixBox, booleanTable, currentKey);

        currentKey = currentKeyGenerator.getStringMatrixKey();

        graphicalDisconnection(stringMatrixBox, stringTable, currentKey);

        currentKey = currentKeyGenerator.getChartKey();

        // graphicalDisconnection(chartBox, chartViewer, currentKey);
        graphicalDisconnection(chartBox, chart, currentKey);
        graphicalDisconnection(chartBox, chartPlayer, currentKey);

        currentKey = currentKeyGenerator.getSpectrumNumberMatrixKey();
        graphicalDisconnection(numberMatrixBox, xAxisConvertor, currentKey);
        graphicalDisconnection(numberMatrixBox, yAxisConvertor, currentKey);

        currentKey = currentKeyGenerator.getImageNumberMatrixKey();
        graphicalDisconnection(numberMatrixBox, imageTable, currentKey);

        graphicalDisconnection(numberMatrixBox, image, currentKey);
        graphicalDisconnection(numberMatrixBox, image2, currentKey);
        graphicalDisconnection(numberMatrixBox, imagePlayer, currentKey);
        changeRefreshingPeriodAction.setKey(null);

        progressBar.setValue(100);
    }

    private <T extends ITarget> void graphicalDisconnection(final AbstractCometeBox<T> box, final T target,
            final IKey key) {
        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                box.disconnectWidget(target, key);
                return null;
            }

            @Override
            protected void done() {
                threadProgressBar.run();
            }
        };
        worker.execute();
    }

    @Override
    public void selectedItemChanged(final EventObject event) {
        if ((event != null) && (event.getSource() instanceof ComboBox)) {
            final ComboBox comboBox = (ComboBox) event.getSource();
            comboBox.setEnabled(false);
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    switchConnections((String) comboBox.getSelectedValue());
                    comboBox.setEnabled(true);
                }
            });
        }
    }

    /**
     * @param args in argument the class name of the DataSource Producer
     */
    public static void main(String[] args) {
        String tmpFile = null;
        boolean debug = false;
        if (args != null) {
            for (String arg : args) {
                if ("debug".equalsIgnoreCase(arg)) {
                    debug = true;
                } else if (tmpFile == null) {
                    tmpFile = arg;
                }
            }
        }
        final String snapshotFile = tmpFile;
        final TestPanel testpanel = new TestPanel(debug);
        final JFrame frame = new JFrame("Comete V2 TestPanel");

        JMenuBar menuBar = new JMenuBar();
        final ComboBox sourceProducerList = new ComboBox();

        sourceProducerList.addComboBoxListener(testpanel);

        sourceProducerList.addItem(SimulatedDataSourceProducer.SOURCE_PRODUCER_ID);
        sourceProducerList.addItem(TangoDataSourceFactory.SOURCE_PRODUCER_ID);

        menuBar.add(sourceProducerList);

        frame.setContentPane(testpanel);
        frame.setJMenuBar(menuBar);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                frame.pack();
                Rectangle bounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
                int maxWidth = bounds.width - 30;
                int maxHeight = bounds.height - 30;
                Dimension size = new Dimension(frame.getWidth(), frame.getHeight());
                if (size.width > maxWidth) {
                    size.width = maxWidth;
                }
                if (size.height > maxHeight) {
                    size.height = maxHeight;
                }
                frame.setSize(size);
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);
                frame.toFront();
                if (snapshotFile != null) {
                    SnapshotUtil.startSnapshot(frame, 10000, snapshotFile);
                }
            }
        });
    }

}
