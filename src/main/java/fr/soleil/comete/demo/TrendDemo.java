package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.definition.event.WheelSwitchEvent;
import fr.soleil.comete.definition.listener.IComboBoxListener;
import fr.soleil.comete.definition.listener.IWheelSwitchListener;
import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.demo.simulated.data.service.TangoKeyGenerator;
import fr.soleil.comete.simulated.data.service.IKeyGenerator;
import fr.soleil.comete.simulated.data.service.SimulatedDataSourceProducer;
import fr.soleil.comete.simulated.data.service.SimulatedKeyGenerator;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.WheelSwitch;
import fr.soleil.comete.tango.data.service.TangoDataSourceFactory;
import fr.soleil.data.service.AbstractRefreshingManager;
import fr.soleil.data.service.DataSourceProducerProvider;
import fr.soleil.data.service.HistoryKey;
import fr.soleil.data.service.IDataSourceProducer;
import fr.soleil.data.service.IKey;
import fr.soleil.data.service.PolledRefreshingStrategy;
import fr.soleil.data.source.HistoryDataSourceProducer;

/**
 * A demo about how to create a trend
 * 
 * @author girardot
 */
public class TrendDemo extends JPanel implements IComboBoxListener {

    private static final long serialVersionUID = 6349404907795505203L;

    // 1000ms = 1s
    private static final PolledRefreshingStrategy REFRESHING_STRATEGY = new PolledRefreshingStrategy(1000);

    private final Chart chartViewer;
    private final ChartViewerBox chartBox;
    private final ComboBox sourceProducerList;
    private final ComboBox dataTypeList;
    private final Object chartLock;
    private final Map<String, IKeyGenerator> keyGeneratorMap;
    private final JPanel southPanel;
    private final JPanel periodPanel;
    private final WheelSwitch periodSwitch;
    private final JLabel periodLabel;

    public TrendDemo() {
        super(new BorderLayout());

        chartLock = new Object();

        // register used producers
        DataSourceProducerProvider.pushNewProducer(HistoryDataSourceProducer.class);
        DataSourceProducerProvider.pushNewProducer(SimulatedDataSourceProducer.class);
        DataSourceProducerProvider.pushNewProducer(TangoDataSourceFactory.class);

        HistoryDataSourceProducer producer = (HistoryDataSourceProducer) DataSourceProducerProvider
                .getProducerByClassName(HistoryDataSourceProducer.class.getName());
        producer.setDefaultRefreshingStrategy(REFRESHING_STRATEGY);

        // key generating system
        keyGeneratorMap = new HashMap<String, IKeyGenerator>();
        IKeyGenerator tangoKey = new TangoKeyGenerator();
        keyGeneratorMap.put(tangoKey.toString(), tangoKey);
        IKeyGenerator simulatedKey = new SimulatedKeyGenerator();
        keyGeneratorMap.put(simulatedKey.toString(), simulatedKey);

        // create components
        chartBox = new ChartViewerBox();
        chartViewer = new Chart();
        // chartViewer.setAnnotation(IChartViewer.TIME_ANNO, IChartViewer.X);
        chartViewer.setAutoHighlightOnLegend(true);

        sourceProducerList = new ComboBox();
        sourceProducerList.addComboBoxListener(this);

        dataTypeList = new ComboBox();
        dataTypeList.setValueList(0, 1, 2, 3);
        dataTypeList.setDisplayedList("Number Scalar", "Boolean Scalar", "Number Spectrum", "Boolean Spectrum");
        dataTypeList.addComboBoxListener(this);

        periodLabel = new JLabel("Refreshing Period (ms)");
        periodSwitch = new WheelSwitch();
        periodSwitch.setMinValue(0);
        periodSwitch.setMaxValue(Integer.MAX_VALUE);
        periodSwitch.setNumberValue(REFRESHING_STRATEGY.getRefreshingPeriod());
        periodSwitch.addWheelSwitchListener(new IWheelSwitchListener() {
            @Override
            public void valueChange(WheelSwitchEvent evt) {
                updateRefreshingPeriod(getKey());
            }
        });
        periodPanel = new JPanel(new BorderLayout());
        periodPanel.add(periodLabel, BorderLayout.WEST);
        periodPanel.add(periodSwitch, BorderLayout.CENTER);

        southPanel = new JPanel(new BorderLayout());
        southPanel.add(dataTypeList, BorderLayout.NORTH);
        southPanel.add(periodPanel, BorderLayout.SOUTH);

        // layout components
        add(chartViewer, BorderLayout.CENTER);
        add(sourceProducerList, BorderLayout.NORTH);
        add(southPanel, BorderLayout.SOUTH);

        // register possible key generators in combobox
        sourceProducerList.addItem(SimulatedDataSourceProducer.SOURCE_PRODUCER_ID);
        sourceProducerList.addItem(TangoDataSourceFactory.SOURCE_PRODUCER_ID);
    }

    /**
     * History key creation
     * 
     * @param key
     *            The {@link IKey} that identifies the source to historize
     */
    private void historize(HistoryKey key) {
        if (key != null) {
            // prepare dataview
            String id = key.getInformationKey();
            chartViewer.setDataViewCometeColor(id, CometeColor.RED);
            chartViewer.setDataViewMarkerCometeColor(id, CometeColor.RED);
            chartViewer.setDataViewMarkerSize(id, 5);
            chartViewer.setDataViewMarkerStyle(id, IChartViewer.MARKER_BOX);
            // connect chart to historized source
            chartBox.connectWidget(chartViewer, key);
            updateRefreshingPeriod(key);
        }
    }

    @Override
    public void selectedItemChanged(EventObject event) {
        synchronized (chartLock) {
            chartBox.disconnectWidgetFromAll(chartViewer);
            chartViewer.setData(null);
            historize(getKey());
        }
    }

    protected void updateRefreshingPeriod(IKey key) {
        if (key != null) {
            IDataSourceProducer producer = DataSourceProducerProvider.getProducer(key.getSourceProduction());
            if (producer instanceof AbstractRefreshingManager<?>) {
                ((AbstractRefreshingManager<?>) producer).setRefreshingStrategy(key,
                        new PolledRefreshingStrategy((int) periodSwitch.getValue()));
                // We transmit refreshing strategy to sub-key
                if (key instanceof HistoryKey) {
                    updateRefreshingPeriod(((HistoryKey) key).getHistory());
                }
            }
        }
    }

    protected HistoryKey getKey() {
        HistoryKey key = null;
        String producerId = (String) sourceProducerList.getSelectedValue();
        IKeyGenerator keyGen = keyGeneratorMap.get(producerId);
        if (keyGen != null) {
            IKey tmp;
            switch (dataTypeList.getSelectedIndex()) {
                case 0:
                    tmp = keyGen.getNumberScalarKey();
                    break;
                case 1:
                    tmp = keyGen.getBooleanScalarKey();
                    break;
                case 2:
                    tmp = keyGen.getSpectrumNumberMatrixKey();
                    break;
                case 3:
                    tmp = keyGen.getBooleanMatrixKey();
                    break;
                default:
                    tmp = null;
                    break;
            }
            // // Prefer using a non-refreshed source: we will use our own
            // // refreshing management
            // if (tmp instanceof TangoKey) {
            // TangoKeyTool.registerRefreshed(tmp, false);
            // }
            key = new HistoryKey(tmp);
        }
        return key;
    }

    public static void main(String[] args) {
        TrendDemo demo = new TrendDemo();
        final JFrame testFrame = new JFrame(TrendDemo.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        testFrame.setContentPane(demo);
        testFrame.setSize(800, 600);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

}
