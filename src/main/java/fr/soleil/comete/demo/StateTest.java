package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.service.IKey;

public class StateTest {

    public static void main(String[] args) {
        String stateAttribute = "tango/tangotest/titan/state";
        if ((args != null) && (args.length > 0) && (args[0] != null) && (!args[0].trim().isEmpty())) {
            stateAttribute = args[0];
        }
        JFrame testFrame = new JFrame(StateTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        final Label stateLabel = new Label();
        final StringScalarBox stringScalarBox = new StringScalarBox();
        final IKey key = new TangoKey();

        TangoKeyTool.registerAttribute(key, stateAttribute);
        JButton connectButton = new JButton("connect/disconnect");
        connectButton.addActionListener(new ActionListener() {
            boolean connected = false;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (connected) {
                    connected = false;
                    stringScalarBox.disconnectWidget(stateLabel, key);
                } else {
                    connected = true;
                    stringScalarBox.connectWidget(stateLabel, key);
                }
            }
        });
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(stateLabel, BorderLayout.CENTER);
        mainPanel.add(connectButton, BorderLayout.SOUTH);
        testFrame.setContentPane(mainPanel);
        testFrame.setSize(300, 100);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);

        IKey key1 = new TangoKey();
        IKey key2 = new TangoKey();
        IKey key3 = new TangoKey();

        TangoKeyTool.registerAttribute(key1, "tango/tangotest/1/state");
        TangoKeyTool.registerAttribute(key2, "tango/tangotest/1/state");
        TangoKeyTool.registerAttribute(key3, "tangotest1/state");

        System.out.println("key1.equals(key2) = " + key1.equals(key2));
        System.out.println("key1.equals(key3) = " + key1.equals(key3));
    }

}
