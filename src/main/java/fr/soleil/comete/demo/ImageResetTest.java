package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.matrixbox.NumberMatrixBox;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.comete.tango.data.service.TangoKey;
import fr.soleil.comete.tango.data.service.TangoKeyTool;
import fr.soleil.data.service.DataSourceProducerProvider;

public class ImageResetTest {

    public static void main(String[] args) {
        final ImageViewer viewer = new ImageViewer();
        viewer.setAlwaysFitMaxSize(true);
        TangoKey key = new TangoKey();
        TangoKeyTool.registerAttribute(key, "tango/tangotest/titan", "double_image_ro");
        NumberMatrixBox box = new NumberMatrixBox();
        box.connectWidget(viewer, key);
        DataSourceProducerProvider.setRefreshingStrategy(key, null);
        JButton button = new JButton("Reset");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                viewer.setNumberMatrix(null);
            }
        });
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(viewer, BorderLayout.CENTER);
        mainPanel.add(button, BorderLayout.SOUTH);
        JFrame frame = new JFrame(ImageResetTest.class.getSimpleName());
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(mainPanel);
        frame.setSize(600, 500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

}
