package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import fr.soleil.comete.definition.widget.IChartViewer;
import fr.soleil.comete.swing.Chart;

public class ChartViewerDoubleDataTest {

    protected static final void updateChartViewer(final IChartViewer viewer, final String id) {
        new Thread("Reset chart") {
            @Override
            public void run() {
                viewer.resetAll(true);
            }
        }.start();
        new Thread("add data1") {
            @Override
            public void run() {
                Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
                int dataCount = 100;
                int points = (int) (Math.random() * dataCount);
                double[][] data = new double[2][points];
                for (int j = 0; j < points; j++) {
                    data[IChartViewer.X_INDEX][j] = (j + 1);
                    data[IChartViewer.Y_INDEX][j] = Math.random() + (Math.random() * 5E9);
                }
                dataMap.put(id, data);
                viewer.addData(dataMap);
            };
        }.start();
        new Thread("Change axis") {
            @Override
            public void run() {
                viewer.setDataViewAxis(id, IChartViewer.Y2);
            };
        }.start();
        new Thread("add data2") {
            @Override
            public void run() {
                Map<String, Object> dataMap = new LinkedHashMap<String, Object>();
                int dataCount = 100;
                int points = (int) (Math.random() * dataCount);
                double[][] data = new double[2][points];
                for (int j = 0; j < points; j++) {
                    data[IChartViewer.X_INDEX][j] = (j + 1);
                    data[IChartViewer.Y_INDEX][j] = Math.random() + (Math.random() * 5E9);
                }
                dataMap.put(id, data);
                viewer.addData(dataMap);
            };
        }.start();
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        final String id = "data01";
        JFrame testFrame = new JFrame(ChartViewerDoubleDataTest.class.getSimpleName());
        testFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setBackground(Color.WHITE);
        final Chart chart = new Chart();
        chart.setManagementPanelVisible(false);
        JButton testButton = new JButton("run test");
        testButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateChartViewer(chart, id);
            }
        });
        final JLabel testLabel = new JLabel("Click many times on button to discover bug", JLabel.CENTER);
        mainPanel.add(testLabel, BorderLayout.NORTH);
        mainPanel.add(chart, BorderLayout.CENTER);
        mainPanel.add(testButton, BorderLayout.SOUTH);
        testFrame.setContentPane(mainPanel);
        testFrame.setSize(750, 550);
        testFrame.setLocationRelativeTo(null);
        testFrame.setVisible(true);
    }

}
