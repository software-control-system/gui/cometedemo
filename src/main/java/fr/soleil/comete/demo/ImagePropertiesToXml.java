package fr.soleil.comete.demo;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.w3c.dom.Node;

import fr.soleil.comete.definition.widget.IFrame;
import fr.soleil.comete.definition.widget.properties.ImageProperties;
import fr.soleil.comete.definition.widget.properties.xml.ImagePropertiesXmlManager;
import fr.soleil.comete.swing.ImageViewer;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;
import fr.soleil.lib.project.xmlhelpers.exception.XMLWarning;

public class ImagePropertiesToXml extends JPanel {

    private static final long serialVersionUID = 571262290517679428L;

    private ImageViewer imageViewer;
    private JTextArea textArea;
    private JButton showPropButton;
    private JButton loadPropButton;
    private JButton clearButton;
    private JButton applyNewData;

    public ImagePropertiesToXml() {
        setLayout(new BorderLayout());
        imageViewer = new ImageViewer();
        imageViewer.setApplicationId("ID");
        textArea = new JTextArea();
        add(imageViewer, BorderLayout.CENTER);
        add(textArea, BorderLayout.SOUTH);
        JPanel buttonPanel = new JPanel();
        showPropButton = new JButton("Show properties");
        showPropButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String prop = ImagePropertiesXmlManager.toXmlString(imageViewer.getImageProperties());
                textArea.setText(prop);

            }
        });

        loadPropButton = new JButton("Load properties");
        loadPropButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String prop = textArea.getText();
                if (prop != null && !prop.isEmpty()) {
                    try {
                        Node node = XMLUtils.getRootNodeFromFileContent(prop);
                        ImageProperties properties = ImagePropertiesXmlManager.loadImageProperties(node);
                        imageViewer.setImageProperties(properties);
                    } catch (XMLWarning e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            }
        });

        clearButton = new JButton("Clear");
        clearButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText("");
            }
        });

        applyNewData = new JButton("apply new Data");
        applyNewData.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                initData();
            }
        });

        buttonPanel.add(showPropButton);
        buttonPanel.add(loadPropButton);
        buttonPanel.add(clearButton);
        buttonPanel.add(applyNewData);

        add(buttonPanel, BorderLayout.NORTH);
        initData();

    }

    private void initData() {
        int width = new Random().nextInt(100) + 1;
        int height = new Random().nextInt(100) + 1;
        double[][] value = new double[height][width];
        for (double[] line : value) {
            for (int i = 0; i < line.length; i++) {
                line[i] = Math.random() * 500;
            }
        }
        imageViewer.setNumberMatrix(value);
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame();
        ImagePropertiesToXml panel = new ImagePropertiesToXml();

        panel.setSize(600, 600);
        panel.setPreferredSize(panel.getSize());
        panel.setEnabled(true);

        frame.setContentPane(panel);
        frame.setSize(600, 600);
        frame.setTitle(panel.getClass().getName());
        frame.setDefaultCloseOperation(IFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }

}
